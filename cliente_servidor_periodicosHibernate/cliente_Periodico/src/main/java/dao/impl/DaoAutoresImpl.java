package dao.impl;

import com.google.gson.Gson;
import config.ConfigurationCliente;
import config.ConfigurationSingleton_OkHttpClient;
import dao.DaoAutores;
import dao.modelo.Autor;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import utils.Constantes;

import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoAutoresImpl implements DaoAutores {

    @Override
    public Either<String,Autor> comprobarExistencia(String nombre, String apellido) {
        AtomicReference<Either<String, Autor>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Autor autor = new Autor();
        autor.setNombre(nombre);
        autor.setApellidos(apellido);
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_AUTOR)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(autor))
                            .onSuccess(userJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_AUTOR, userJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_COMPROBAR_EXIXTENCIA_AUTOR);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            Autor autor1 = gson.fromJson(respuestaString, Autor.class);
                                                            resultado.set(Either.right(autor1));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();    }


    @Override
    public Either<String, Boolean> addAutor(Autor a) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        FormBody.Builder b = new FormBody.Builder();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_AUTOR)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(a))
                            .onSuccess(autorJson -> {
                                b.add(Constantes.PARAM_AUTOR, autorJson);
                                b.add(Constantes.PARAM_ACCION, Constantes.URL_ADD_AUTOR);
                                RequestBody formBody = b.build();
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .post(formBody)
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();    }
}
