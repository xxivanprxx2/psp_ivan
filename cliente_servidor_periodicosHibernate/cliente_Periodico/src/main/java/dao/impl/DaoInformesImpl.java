package dao.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import config.ConfigurationCliente;
import config.ConfigurationSingleton_OkHttpClient;
import dao.DaoInformes;
import dao.modelo.*;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import utils.Constantes;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoInformesImpl implements DaoInformes {
    @Override
    public Either<String, InformeTipo1> ratingMedioPeriodicos(int id) {
        AtomicReference<Either<String, InformeTipo1>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        Periodico periodico = Periodico.builder()
                .id(id)
                .build();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_INFORME)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(periodico))
                            .onSuccess(periodicoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_RATING_PERIODICOS);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            InformeTipo1 media = gson.fromJson(respuestaString, InformeTipo1.class);
                                                            resultado.set(Either.right(media));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, InformeTipo1> ratingMedioAutor(int id) {
        AtomicReference<Either<String, InformeTipo1>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        Autor autor = Autor.builder()
                .id(id)
                .build();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_INFORME)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(autor))
                            .onSuccess(autorJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_AUTOR, autorJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_RATING_AUTOR);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            InformeTipo1 media = gson.fromJson(respuestaString, InformeTipo1.class);
                                                            resultado.set(Either.right(media));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).

                onFailure(throwable ->

                {
                    resultado.set(Either.left("Error al conectar con el servidor"));
                    log.error(throwable.getMessage(), throwable);
                });
        return resultado.get();
    }

    @Override
    public Either<String, List<InformeTipo1>> listadoAutores() {
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<InformeTipo1>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_INFORME)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    HttpUrl.Builder urBuilder
                            = HttpUrl.parse(String.valueOf(url))
                            .newBuilder();
                    urBuilder
                            .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_LISTAR_AUTORES);
                    url.set(urBuilder
                            .build().toString());
                    Request request = new Request.Builder()
                            .url(url.get())
                            .build();
                    Try.of(() -> okHttpClient.newCall(request).execute())
                            .onSuccess(response -> {
                                Try.of(() -> response.body().string())
                                        .onSuccess(respuestaString -> {
                                            Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<InformeTipo1>>() {
                                            }.getType()))
                                                    .map(o -> (List) o)
                                                    .onSuccess(o -> resultado.set(Either.right(o)))
                                                    .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Autor>> listadoAutoresValor(double valor) {
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Autor>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();

        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_INFORME)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(valor))
                            .onSuccess(valorJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_VALOR, valorJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_LISTAR_AUTORES_VALOR);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Periodico>>() {
                                                        }.getType()))
                                                                .map(o -> (List) o)
                                                                .onSuccess(o -> resultado.set(Either.right(o)))
                                                                .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, InformeTipo1> autoresMasRting() {
        AtomicReference<Either<String, InformeTipo1>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_INFORME)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    HttpUrl.Builder urBuilder
                            = HttpUrl.parse(String.valueOf(url))
                            .newBuilder();
                    urBuilder
                            .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_AUTORS_MAS_RATING);
                    url.set(urBuilder
                            .build().toString());
                    Request request = new Request.Builder()
                            .url(url.get())
                            .build();
                    Try.of(() -> okHttpClient.newCall(request).execute())
                            .onSuccess(response -> {
                                Try.of(() -> response.body().string())
                                        .onSuccess(respuestaString -> {
                                            if (response.isSuccessful()) {
                                                InformeTipo1 informe = gson.fromJson(respuestaString, InformeTipo1.class);
                                                resultado.set(Either.right(informe));
                                            } else {
                                                resultado.set(Either.left(response.code() + " " + respuestaString));
                                            }
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<InformeTipo1>> tipoArticulosOrdenados() {
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<InformeTipo1>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_INFORME)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    HttpUrl.Builder urBuilder
                            = HttpUrl.parse(String.valueOf(url))
                            .newBuilder();
                    urBuilder
                            .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_TIPOS_ARTICULOS_ORDENADOS);
                    url.set(urBuilder
                            .build().toString());
                    Request request = new Request.Builder()
                            .url(url.get())
                            .build();
                    Try.of(() -> okHttpClient.newCall(request).execute())
                            .onSuccess(response -> {
                                Try.of(() -> response.body().string())
                                        .onSuccess(respuestaString -> {
                                            Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<InformeTipo1>>() {
                                            }.getType()))
                                                    .map(o -> (List) o)
                                                    .onSuccess(o -> resultado.set(Either.right(o)))
                                                    .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, InformesTipo2> periodicoMasSuscripcionesDia(LocalDate date) {
        AtomicReference<Either<String, InformesTipo2>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();

        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_INFORME)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(date))
                            .onSuccess(dateJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_VALOR, dateJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_PERIODICO_MAS_DIA);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            InformesTipo2 informe = gson.fromJson(respuestaString, InformesTipo2.class);
                                                            resultado.set(Either.right(informe));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, InformesTipo3> periodicoMasGanames() {
        AtomicReference<Either<String, InformesTipo3>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();

        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    HttpUrl.Builder urBuilder
                            = HttpUrl.parse(String.valueOf(url))
                            .newBuilder();
                    urBuilder
                            .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_PERIODICO_MAS_MES);
                    url.set(urBuilder
                            .build().toString());
                    Request request = new Request.Builder()
                            .url(url.get())
                            .build();
                    Try.of(() -> okHttpClient.newCall(request).execute())
                            .onSuccess(response -> {
                                Try.of(() -> response.body().string())
                                        .onSuccess(respuestaString -> {
                                            if (response.isSuccessful()) {
                                                InformesTipo3 informe = gson.fromJson(respuestaString, InformesTipo3.class);
                                                resultado.set(Either.right(informe));
                                            } else {
                                                resultado.set(Either.left(response.code() + " " + respuestaString));
                                            }
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
}
