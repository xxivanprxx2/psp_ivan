package dao.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import config.ConfigurationCliente;
import config.ConfigurationSingleton_OkHttpClient;
import dao.DaoArticulos;
import dao.modelo.Articulo;
import dao.modelo.Periodico;
import dao.modelo.Tipo_articulo;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import utils.Constantes;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoArticulosImpl implements DaoArticulos {
    @Override
    public Either<String, Boolean> addArticulo(Articulo a) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        FormBody.Builder b = new FormBody.Builder();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_ARTICULO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(a))
                            .onSuccess(articuloJson -> {
                                b.add(Constantes.PARAM_ARTICULO, articuloJson);
                                b.add(Constantes.PARAM_ACCION, Constantes.URL_ADD_ARTICULO);
                                RequestBody formBody = b.build();
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .post(formBody)
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Tipo_articulo>> devolverTodosTiposArticulos() {
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Tipo_articulo>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_ARTICULO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    HttpUrl.Builder urBuilder
                            = HttpUrl.parse(String.valueOf(url))
                            .newBuilder();
                    urBuilder
                            .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_TIPOS_ARTICULOS);
                    url.set(urBuilder
                            .build().toString());
                    Request request = new Request.Builder()
                            .url(url.get())
                            .build();
                    Try.of(() -> okHttpClient.newCall(request).execute())
                            .onSuccess(response -> {
                                Try.of(() -> response.body().string())
                                        .onSuccess(respuestaString -> {
                                            Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Tipo_articulo>>() {
                                            }.getType()))
                                                    .map(o -> (List) o)
                                                    .onSuccess(o -> resultado.set(Either.right(o)))
                                                    .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }


    @Override
    public Either<String, List<Articulo>> devolverTodosArticulosPeriodico(int idPeriodico) {
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Articulo>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Periodico periodico = Periodico.builder()
                .id(idPeriodico)
                .build();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_ARTICULO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(periodico))
                            .onSuccess(periodicoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_ARTICULOS_PERIODICO);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Articulo>>() {
                                                        }.getType()))
                                                                .map(o -> (List) o)
                                                                .onSuccess(o -> resultado.set(Either.right(o)))
                                                                .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
}
