package dao.impl;

import com.google.gson.Gson;
import config.ConfigurationCliente;
import config.ConfigurationSingleton_OkHttpClient;
import dao.DaoArticulosLeidos;
import dao.modelo.Articulo;
import dao.modelo.ArticuloLeido;
import dao.modelo.Lector;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import utils.Constantes;

import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoArticulosLeidosImpl implements DaoArticulosLeidos {
    @Override
    public Either<String, Boolean> addArticuloLeido(ArticuloLeido a) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        FormBody.Builder b = new FormBody.Builder();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_ARTICULO_LEIDO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(a))
                            .onSuccess(articuloLeidoJson -> {
                                b.add(Constantes.PARAM_ARTICULO_LEIDO, articuloLeidoJson);
                                b.add(Constantes.PARAM_ACCION, Constantes.URL_ADD_ARTICULO_LEIDO);
                                RequestBody formBody = b.build();
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .post(formBody)
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, ArticuloLeido> devolverArticuloLeido(int id_Articulo, long id_Lector) {

        AtomicReference<Either<String, ArticuloLeido>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Articulo articulo = new Articulo();
        articulo.setId(id_Articulo);
        Lector lector = new Lector();
        lector.setId_lector((int)id_Lector);
        ArticuloLeido articuloLeido = ArticuloLeido.builder()
                .articulo(articulo)
                .lector(lector)
                .build();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_ARTICULO_LEIDO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(articuloLeido))
                            .onSuccess(articuloLeidoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_ARTICULO_LEIDO, articuloLeidoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_ARTICULO_LEIDO);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            ArticuloLeido articuloLeido1 = gson.fromJson(respuestaString, ArticuloLeido.class);
                                                            resultado.set(Either.right(articuloLeido1));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();    }

    @Override
    public Either<String, Boolean> actualizarArticuloLeido(int rating, ArticuloLeido a) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        a.setRating(rating);
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_ARTICULO_LEIDO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(a))
                            .onSuccess(articuloLeidoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_ARTICULO_LEIDO, articuloLeidoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_ACTUALIZAR_ARTICULO_LEIDO);
                                String urlParametros = urBuilder.build().toString();
                                Request request = new Request.Builder()
                                        .url(urlParametros)
                                        .put(RequestBody.create(""
                                                , MediaType.get("application/json")))
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
}
