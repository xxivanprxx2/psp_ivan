package dao.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import config.ConfigurationCliente;
import config.ConfigurationSingleton_OkHttpClient;
import dao.DaoPeriodicos;
import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import utils.Constantes;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoPeriodicosImpl implements DaoPeriodicos {
    @Override
    public Either<String, Boolean> borrarPeriodico(Periodico periodico) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(periodico))
                            .onSuccess(periodicoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_BORRAR_PERIODICO);
                                String urlParametros = urBuilder.build().toString();
                                Request request = new Request.Builder()
                                        .url(urlParametros)
                                        .put(RequestBody.create(""
                                                , MediaType.get("application/json")))
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> addPeriodico(Periodico p) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        FormBody.Builder b = new FormBody.Builder();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(p))
                            .onSuccess(periodicoJson -> {
                                b.add(Constantes.PARAM_PERIODICO, periodicoJson);
                                b.add(Constantes.PARAM_ACCION, Constantes.URL_ADD_PERIODICO);
                                RequestBody formBody = b.build();
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .post(formBody)
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Periodico>> getTodosPeriodicosUsuario(Usuario u) {

        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Periodico>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(u))
                            .onSuccess(userJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_USUARIO, userJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_PERIODICOS_USER);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Periodico>>() {
                                                        }.getType()))
                                                                .map(o -> (List) o)
                                                                .onSuccess(o -> resultado.set(Either.right(o)))
                                                                .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Periodico>> getTodosPeriodicos() {

        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Periodico>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    HttpUrl.Builder urBuilder
                            = HttpUrl.parse(String.valueOf(url))
                            .newBuilder();
                    urBuilder
                            .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_PERIODICOS);
                    url.set(urBuilder
                            .build().toString());
                    Request request = new Request.Builder()
                            .url(url.get())
                            .build();
                    Try.of(() -> okHttpClient.newCall(request).execute())
                            .onSuccess(response -> {
                                Try.of(() -> response.body().string())
                                        .onSuccess(respuestaString -> {
                                            Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Periodico>>() {
                                            }.getType()))
                                                    .map(o -> (List) o)
                                                    .onSuccess(o -> resultado.set(Either.right(o)))
                                                    .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
            resultado.set(Either.left("Error al ejecutar"));
            log.error(throwable.getMessage(), throwable);
        });
    }).
    onFailure(throwable ->{
        resultado.set(Either.left("Error al conectar con el servidor"));
        log.error(throwable.getMessage(), throwable);
    });
        return resultado.get();
}

    @Override
    public Either<String, Boolean> comprobarExistenciaporId(int id) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Periodico periodico = Periodico.builder()
                .id(id)
                .build();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(periodico))
                            .onSuccess(periodicoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_COMPROBAR_ID_PERIODICO);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> actualizarPeriodico(Periodico periodico) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(periodico))
                            .onSuccess(periodicoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_ACTUALIZAR_PERIODICO);
                                String urlParametros = urBuilder.build().toString();
                                Request request = new Request.Builder()
                                        .url(urlParametros)
                                        .put(RequestBody.create(""
                                                , MediaType.get("application/json")))
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, Periodico> devolverPeriodico(int id) {
        AtomicReference<Either<String, Periodico>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Periodico periodico = Periodico.builder()
                .id(id)
                .build();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_PERIODICO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(periodico))
                            .onSuccess(periodicoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_PERIODICO);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            Periodico periodico1 = gson.fromJson(respuestaString, Periodico.class);
                                                            resultado.set(Either.right(periodico1));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
}
