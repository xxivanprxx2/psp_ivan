package dao.impl;

import com.google.gson.Gson;
import config.ConfigurationCliente;
import config.ConfigurationSingleton_OkHttpClient;
import dao.DaoLectores;
import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import utils.Constantes;

import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoLectoresImpl implements DaoLectores {

    @Override
    public Either<String, Boolean> addLector(Lector l, Usuario u) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        FormBody.Builder b = new FormBody.Builder();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_USUARIO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(l))
                            .onSuccess(lectorJson -> {
                                            b.add(Constantes.PARAM_LECTOR, lectorJson);
                                            b.add(Constantes.PARAM_ACCION, Constantes.URL_ADD_LECTOR);
                                            RequestBody formBody = b.build();
                                            Request request = new Request.Builder()
                                                    .url(url.get())
                                                    .post(formBody)
                                                    .build();
                                            Try.of(() -> okHttpClient.newCall(request).execute())
                                                    .onSuccess(response -> {
                                                        Try.of(() -> response.body().string())
                                                                .onSuccess(respuestaString -> {
                                                                    if (response.isSuccessful()) {
                                                                        resultado.set(Either.right(true));
                                                                    } else {
                                                                        resultado.set(Either.left(response.code() + " " + respuestaString));
                                                                    }
                                                                }).onFailure(throwable -> {
                                                            resultado.set(Either.left("Error"));
                                                            log.error(throwable.getMessage(), throwable);
                                                        });
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error al ejecutar"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, Lector> devolverLector(long id) {
        AtomicReference<Either<String, Lector>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Lector lector = Lector.builder()
                .id_lector((int)id)
                .build();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_USUARIO)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(lector))
                            .onSuccess(userJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_LECTOR, userJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_LECTOR);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            Lector lector1 = gson.fromJson(respuestaString, Lector.class);
                                                            resultado.set(Either.right(lector1));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();    }
}