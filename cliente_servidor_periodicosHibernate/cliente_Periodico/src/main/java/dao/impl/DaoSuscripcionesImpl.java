package dao.impl;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import config.ConfigurationCliente;
import config.ConfigurationSingleton_OkHttpClient;
import dao.DaoPeriodicos;
import dao.DaoSuscripciones;
import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import utils.Constantes;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoSuscripcionesImpl implements DaoSuscripciones {
    @Inject
    private DaoPeriodicos daoPeriodicos;

    @Override
    public Either<String, Boolean> addSuscripcion(Suscripcion s) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        FormBody.Builder b = new FormBody.Builder();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_SUSCRIPCION)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(s))
                            .onSuccess(suscripcionJson -> {
                                b.add(Constantes.PARAM_SUSCRIPCION, suscripcionJson);
                                b.add(Constantes.PARAM_ACCION, Constantes.URL_ADD_SUSCRIPCION);
                                RequestBody formBody = b.build();
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .post(formBody)
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        if (response.isSuccessful()) {
                                                            resultado.set(Either.right(true));
                                                        } else {
                                                            resultado.set(Either.left(response.code() + " " + respuestaString));
                                                        }
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al ejecutar"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Periodico>> devolverPeridicosSuscripcionesUsuario(Lector l) {
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Periodico>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_SUSCRIPCION)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(l))
                            .onSuccess(userJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_LECTOR, userJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_PERIODICOS_SUSCRIPCIONES_USUARIO);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Periodico>>() {
                                                        }.getType()))
                                                                .map(o -> (List) o)
                                                                .onSuccess(o -> resultado.set(Either.right(o)))
                                                                .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Suscripcion>> devolverTodasSuscripcionesUsuario(Lector l) {
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Suscripcion>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_SUSCRIPCION)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(l))
                            .onSuccess(userJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_LECTOR, userJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_SUSCRIPCIONES_USUARIO);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Suscripcion>>() {
                                                        }.getType()))
                                                                .map(o -> (List) o)
                                                                .onSuccess(o -> resultado.set(Either.right(o)))
                                                                .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Suscripcion>> devolverTodasSuscripciones(Periodico p) {

        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        AtomicReference<Either<String, List<Suscripcion>>> resultado = new AtomicReference<>();
        FormBody.Builder b = new FormBody.Builder();
        Gson gson = new Gson();
        AtomicReference<String> url = new AtomicReference<>();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_SUSCRIPCION)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(p))
                            .onSuccess(periodicoJson -> {
                                HttpUrl.Builder urBuilder
                                        = HttpUrl.parse(String.valueOf(url))
                                        .newBuilder();
                                urBuilder
                                        .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                        .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_DEVOLVER_TODAS_SUSCRIPCIONES);
                                url.set(urBuilder
                                        .build().toString());
                                Request request = new Request.Builder()
                                        .url(url.get())
                                        .build();
                                Try.of(() -> okHttpClient.newCall(request).execute())
                                        .onSuccess(response -> {
                                            Try.of(() -> response.body().string())
                                                    .onSuccess(respuestaString -> {
                                                        Try.of(() -> gson.fromJson(respuestaString, new TypeToken<List<Suscripcion>>() {
                                                        }.getType()))
                                                                .map(o -> (List) o)
                                                                .onSuccess(o -> resultado.set(Either.right(o)))
                                                                .onFailure(throwable -> resultado.set(Either.left("No se pudo parsear")));
                                                    }).onFailure(throwable -> {
                                                resultado.set(Either.left("Error"));
                                                log.error(throwable.getMessage(), throwable);
                                            });
                                        }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al ejecutar"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> borrarSuscricion(Usuario u, Periodico p) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference<String> url = new AtomicReference<>();
        OkHttpClient okHttpClient = ConfigurationSingleton_OkHttpClient.getInstance();
        Gson gson = new Gson();
        Try.of(() -> ConfigurationCliente.getInstance().getPath_base() + Constantes.PARAM_SUSCRIPCION)
                .onSuccess(urlReference -> {
                    url.set(urlReference);
                    Try.of(() -> gson.toJson(u))
                            .onSuccess(userJson -> {
                                Try.of(() -> gson.toJson(p))
                                .onSuccess(periodicoJson -> {
                                    HttpUrl.Builder urBuilder
                                            = HttpUrl.parse(String.valueOf(url))
                                            .newBuilder();
                                    urBuilder
                                            .addQueryParameter(Constantes.PARAM_USUARIO, userJson)
                                            .addQueryParameter(Constantes.PARAM_PERIODICO, periodicoJson)
                                            .addQueryParameter(Constantes.PARAM_ACCION, Constantes.URL_BORRAR_SUSCRIPCION);
                                    String urlParametros = urBuilder.build().toString();
                                    Request request = new Request.Builder()
                                            .url(urlParametros)
                                            .put(RequestBody.create(""
                                                    , MediaType.get("application/json")))
                                            .build();
                                    Try.of(() -> okHttpClient.newCall(request).execute())
                                            .onSuccess(response -> {
                                                Try.of(() -> response.body().string())
                                                        .onSuccess(respuestaString -> {
                                                            if (response.isSuccessful()) {
                                                                resultado.set(Either.right(true));
                                                            } else {
                                                                resultado.set(Either.left(response.code() + " " + respuestaString));
                                                            }
                                                        }).onFailure(throwable -> {
                                                    resultado.set(Either.left("Error"));
                                                    log.error(throwable.getMessage(), throwable);
                                                });
                                            }).onFailure(throwable -> {
                                        resultado.set(Either.left("Error al ejecutar"));
                                        log.error(throwable.getMessage(), throwable);
                                    });
                                }).onFailure(throwable -> {
                                    resultado.set(Either.left("Error al convertir a json"));
                                    log.error(throwable.getMessage(), throwable);
                                });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left("Error al convertir a json"));
                        log.error(throwable.getMessage(), throwable);
                    });
                }).onFailure(throwable -> {
            resultado.set(Either.left("Error al conectar con el servidor"));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
}
