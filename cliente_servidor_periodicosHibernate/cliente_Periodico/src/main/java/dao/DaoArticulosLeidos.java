package dao;

import dao.modelo.ArticuloLeido;
import io.vavr.control.Either;

public interface DaoArticulosLeidos {
    Either<String, Boolean> addArticuloLeido(ArticuloLeido a);

    Either<String, ArticuloLeido> devolverArticuloLeido(int id_Articulo, long id_Lector);

    Either<String, Boolean> actualizarArticuloLeido(int rating, ArticuloLeido a);
}
