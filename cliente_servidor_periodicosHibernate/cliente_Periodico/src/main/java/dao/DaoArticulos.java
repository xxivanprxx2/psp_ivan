package dao;

import dao.modelo.Articulo;
import dao.modelo.Tipo_articulo;
import io.vavr.control.Either;

import java.util.List;

public interface DaoArticulos {
    Either<String, Boolean> addArticulo(Articulo a);

    Either<String, List<Tipo_articulo>> devolverTodosTiposArticulos();


    Either<String, List<Articulo>> devolverTodosArticulosPeriodico(int idPeriodico);

}
