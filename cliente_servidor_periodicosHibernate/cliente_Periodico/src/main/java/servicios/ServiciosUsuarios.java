package servicios;

import dao.modelo.Usuario;
import io.vavr.control.Either;
import lombok.SneakyThrows;

import java.util.List;

public interface ServiciosUsuarios {
    static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    @SneakyThrows
    Either<String, Usuario> comprobarUsuario(String usuario, String pass);

    @SneakyThrows
    Either<String, Usuario> comprobarUsuarioUser(String usuario);

    @SneakyThrows
    String hashearPass(Usuario usuario);

    Either<String, Boolean> actualizarUsuario(Usuario usuario);

    Either<String,List<Usuario>> devolverTodosusuarios();

    Either<String, Boolean> borrarUsuario(Usuario usuario);

    Either<String, Boolean> addUsuario(Usuario usuario);
    Either<String, Boolean> logout(Usuario usuario);
}
