package servicios;

import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.util.List;

public interface ServiciosSuscripciones {
    Either<String, Boolean> addSuscripcion(Suscripcion suscripcion);

    Either<String, List<Periodico>> devolverPeriodicosSuscripcionesLector(Lector lector);

    Either<String, List<Suscripcion>> devolverSuscripcionesLector(Lector lector);

    Either<String, List<Suscripcion>> devolverSuscripcionesPeriodico(Periodico periodico);

    Either<String, Boolean> borrarSuscripcion(Usuario u, Periodico p);
}
