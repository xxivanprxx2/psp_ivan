package servicios.impl;

import dao.DaoArticulos;
import dao.modelo.Articulo;
import dao.modelo.Tipo_articulo;
import io.vavr.control.Either;
import servicios.ServiciosArticulos;

import javax.inject.Inject;
import java.util.List;

public class ServiciosArticulosImpl implements ServiciosArticulos {
    @Inject
    DaoArticulos daoArticulos;

    @Override
    public Either<String, List<Tipo_articulo>> devolerTodosTiposArticulos(){

        return daoArticulos.devolverTodosTiposArticulos();
    }


    @Override
    public Either<String, Boolean> addArticulo(Articulo articulo){
        return daoArticulos.addArticulo(articulo);
    }


    @Override
    public Either<String, List<Articulo>> devolverArticulosPeriodico(int id){
        return daoArticulos.devolverTodosArticulosPeriodico(id);
    }
}
