package servicios.impl;

import dao.DaoArticulosLeidos;
import dao.modelo.ArticuloLeido;
import io.vavr.control.Either;
import servicios.ServiciosArticulosLeidos;

import javax.inject.Inject;

public class ServiciosArticulosLeidosImpl implements ServiciosArticulosLeidos {

    @Inject
    DaoArticulosLeidos daoArticulosLeidos;
    @Override
    public Either<String, Boolean> addArticulo(ArticuloLeido articuloLeido){
        return  daoArticulosLeidos.addArticuloLeido(articuloLeido);
    }

    @Override
    public Either<String, ArticuloLeido> devolverArticuloLeido(int idArticulo, long idLector){
        return daoArticulosLeidos.devolverArticuloLeido(idArticulo,idLector);
    }


    @Override
    public Either<String, Boolean> actualizarRating(int rating, ArticuloLeido a){
        return daoArticulosLeidos.actualizarArticuloLeido(rating, a);
    }

}
