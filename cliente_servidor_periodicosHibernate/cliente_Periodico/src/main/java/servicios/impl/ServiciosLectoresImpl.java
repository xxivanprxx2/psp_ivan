package servicios.impl;

import dao.DaoLectores;
import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosLectores;

import javax.inject.Inject;

public class ServiciosLectoresImpl implements ServiciosLectores {
    @Inject
    DaoLectores daoLectores;
    @Override
    public Either<String,Boolean> addLector(Lector lector, Usuario usuario){
        return daoLectores.addLector(lector, usuario);
    }
    @Override
    public Either<String, Lector> devolverUnLector(long id){
        return daoLectores.devolverLector(id);
    }
}
