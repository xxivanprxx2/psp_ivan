package servicios.impl;

import dao.DaoAutores;
import dao.modelo.Autor;
import io.vavr.control.Either;
import servicios.ServiciosAutores;

import javax.inject.Inject;

public class ServiciosAutoresImpl implements ServiciosAutores {
    @Inject
    DaoAutores daoAutores;


    @Override
    public Either<String,Autor> comprobarExistencia(String nombre, String apellido){
        return daoAutores.comprobarExistencia(nombre,apellido);
    }

    @Override
    public Either<String, Boolean> addAutor(Autor a){

        return daoAutores.addAutor(a);
    }

}
