package servicios.impl;

import dao.DaoUsuarios;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import lombok.SneakyThrows;
import servicios.ServiciosUsuarios;

import javax.inject.Inject;
import java.security.MessageDigest;
import java.util.List;

public class ServiciosUsuariosImpl implements ServiciosUsuarios {

    @Inject
    DaoUsuarios daoUsuarios;

    @Override
    @SneakyThrows
    public Either<String, Usuario> comprobarUsuario(String usuario, String pass){
        MessageDigest md = MessageDigest.getInstance("SHA3-512");
        byte[] hashBytes = md.digest(pass.getBytes());
        var contraseñaHashed = ServiciosUsuarios.bytesToHex(hashBytes);
        return daoUsuarios.getUsuario(usuario, contraseñaHashed);
    }
    @SneakyThrows
    public Either<String, Usuario> comprobarUsuarioUser(String usuario){
        return daoUsuarios.getUsuarioUser(usuario);
    }
    @Override
    @SneakyThrows
    public String hashearPass(Usuario usuario){
        MessageDigest md = MessageDigest.getInstance("SHA3-512");
        byte[] hashBytes = md.digest(usuario.getPassword().getBytes());
        var contraseñaHashed = ServiciosUsuarios.bytesToHex(hashBytes);
        return contraseñaHashed;
    }

    @Override
    public Either<String, Boolean> actualizarUsuario(Usuario usuario){
        return daoUsuarios.actualizarUsuario(usuario);
    }

    @Override
    public Either<String, List<Usuario>> devolverTodosusuarios(){
        return daoUsuarios.getTodosUsuarios();
    }
    @Override
    public Either<String, Boolean> borrarUsuario(Usuario usuario){
        return daoUsuarios.borrarUsuario(usuario);
    }

    @Override
    public Either<String, Boolean> addUsuario(Usuario usuario){
        return daoUsuarios.addUsuario(usuario);
    }
    @Override
    public Either<String, Boolean> logout(Usuario usuario){
        return daoUsuarios.logOut(usuario);
    }

}
