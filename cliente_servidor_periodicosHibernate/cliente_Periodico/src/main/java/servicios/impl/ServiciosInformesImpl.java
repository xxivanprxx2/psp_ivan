package servicios.impl;

import dao.DaoInformes;
import dao.modelo.*;
import io.vavr.control.Either;
import servicios.ServiciosInformes;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;

public class ServiciosInformesImpl implements ServiciosInformes {
    @Inject
    DaoInformes daoInformes;
    @Override
    public Either<String, InformeTipo1> ratingMedioPeriodicos(int id) {
       return daoInformes.ratingMedioPeriodicos(id);
    }
    @Override
    public Either<String, InformeTipo1> ratingMedioAutor(int id) {
        return daoInformes.ratingMedioAutor(id);
    }
    @Override
    public Either<String, List<InformeTipo1>> listadoAutores() {
        return daoInformes.listadoAutores();
    }
    @Override
    public Either<String, List<Autor>> listadoAutoresValor(double valor) {
        return daoInformes.listadoAutoresValor(valor);
    }
    @Override
    public Either<String, InformeTipo1> autoresMasRting() {
        return daoInformes.autoresMasRting();

    }
    @Override
    public Either<String, List<InformeTipo1>> tipoArticulosOrdenados() {
        return daoInformes.tipoArticulosOrdenados();
    }
    @Override
    public Either<String, InformesTipo2> periodicoMasSuscripcionesDia(LocalDate date) {
        return daoInformes.periodicoMasSuscripcionesDia(date);
    }
    @Override
    public Either<String, InformesTipo3> periodicoMasGanames() {
        return daoInformes.periodicoMasGanames();
    }
}
