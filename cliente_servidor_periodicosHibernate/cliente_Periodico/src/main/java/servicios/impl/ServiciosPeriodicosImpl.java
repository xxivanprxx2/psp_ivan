package servicios.impl;

import dao.DaoPeriodicos;
import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosPeriodicos;

import javax.inject.Inject;
import java.util.List;

public class ServiciosPeriodicosImpl implements ServiciosPeriodicos {
    @Inject
    DaoPeriodicos daoPeriodicos;

    @Override
    public Either<String, Boolean> addPeriodico(Periodico periodico) {
        return daoPeriodicos.addPeriodico(periodico);
    }

    @Override
    public Either<String, List<Periodico>> devolverTodosPeriodicosAdmin(Usuario user) {
        return daoPeriodicos.getTodosPeriodicosUsuario(user);
    }

    @Override
    public Either<String, Boolean> comprobarExistenciaPorId(int id) {
        return daoPeriodicos.comprobarExistenciaporId(id);
    }

    @Override
    public Either<String, List<Periodico>> devolverTodosPeriodicos(){
        return daoPeriodicos.getTodosPeriodicos();
    }

    @Override
    public Either<String, Periodico> devolverPeriodico(int id){
        return daoPeriodicos.devolverPeriodico(id);
    }

    @Override
    public Either<String, Boolean> actualizarPeriodico(Periodico periodico){
        return daoPeriodicos.actualizarPeriodico(periodico);
    }
    @Override
    public Either<String, Boolean> borrarPeriodico(Periodico periodico){
        return daoPeriodicos.borrarPeriodico(periodico);
    }
}
