package gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.IOException;

public class MainFx{
    @Inject
    FXMLLoader fxmlLoader;

    public void start(@Observes @StartupScene Stage primaryStage){
        try {
            Parent fxmlParent = fxmlLoader.load(getClass().getResourceAsStream("/fxml/logging.fxml"));
            primaryStage.setScene(new Scene(fxmlParent, 300, 100));
            primaryStage.setTitle("Periodicos Ivan");
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
