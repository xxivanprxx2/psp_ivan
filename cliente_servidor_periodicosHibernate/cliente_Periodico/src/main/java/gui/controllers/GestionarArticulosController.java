package gui.controllers;

import dao.modelo.Articulo;
import dao.modelo.Autor;
import dao.modelo.Periodico;
import dao.modelo.Tipo_articulo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import servicios.ServiciosArticulos;
import servicios.ServiciosAutores;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class GestionarArticulosController implements Initializable {
    @Inject
    ServiciosArticulos serviciosArticulos;
    @Inject
    ServiciosAutores serviciosAutores;
    @FXML
    private TextField textTitular;
    @FXML
    private TextArea textDescripcion;
    @FXML
    private ComboBox<Tipo_articulo> comboTipoArticulo;
    @FXML
    private ComboBox<Periodico> comboPeriodicos;

    @FXML
    private TextField textNombreAutor;
    @FXML
    private TextField textApellidoAutor;

    public ComboBox<Tipo_articulo> getComboTipoArticulo() {
        return comboTipoArticulo;
    }

    public ComboBox<Periodico> getComboPeriodicos() {
        return comboPeriodicos;
    }


    private Alert alertError;
    private Alert alertInfo;
    private boolean añadido = false;
    public void botonAceptar(ActionEvent actionEvent) {
        if (!textDescripcion.getText().isBlank() && !textDescripcion.getText().isEmpty()) {
            if (comboTipoArticulo.getSelectionModel().getSelectedItem() != null) {
                if (comboPeriodicos.getSelectionModel().getSelectedItem() != null) {
                    if (!textNombreAutor.getText().isBlank() && !textNombreAutor.getText().isEmpty()) {
                        if (!textApellidoAutor.getText().isBlank() && !textApellidoAutor.getText().isEmpty()) {
                            Autor autor = serviciosAutores.comprobarExistencia(textNombreAutor.getText(), textApellidoAutor.getText()).get();
                            if (autor != null) {
                                Articulo articulo = new Articulo();
                                Periodico periodico = new Periodico();
                                periodico.setId(comboPeriodicos.getValue().getId());
                                Tipo_articulo tipo_articulo = new Tipo_articulo();
                                tipo_articulo.setId(comboTipoArticulo.getValue().getId());
                                Autor autor1 = new Autor();
                                autor1.setId(autor.getId());
                                articulo.setTitular(textTitular.getText());
                                articulo.setDescripcion(textDescripcion.getText());
                                articulo.setPeriodico(periodico);
                                articulo.setTipo_articulo(tipo_articulo);
                                articulo.setAutor(autor1);
                                añadido = serviciosArticulos.addArticulo(articulo).get();
                                if (!añadido) {
                                    alertError.setContentText("Error al añadir el articulo");
                                    alertError.show();
                                } else {
                                    alertInfo.setContentText("Articulo añadido");
                                    alertInfo.show();
                                }
                            } else {
                                Articulo articulo = new Articulo();
                                Periodico periodico = new Periodico();
                                periodico.setId(comboPeriodicos.getValue().getId());
                                Tipo_articulo tipo_articulo = new Tipo_articulo();
                                tipo_articulo.setId(comboTipoArticulo.getValue().getId());
                                Autor autor1 = new Autor();
                                autor1.setId(autor.getId());
                                articulo.setTitular(textTitular.getText());
                                articulo.setDescripcion(textDescripcion.getText());
                                articulo.setPeriodico(periodico);
                                articulo.setTipo_articulo(tipo_articulo);
                                articulo.setAutor(autor1);
                                autor1 = new Autor();
                                autor1.setNombre(textNombreAutor.getText());
                                autor1.setApellidos(textApellidoAutor.getText());
                                if(serviciosAutores.addAutor(autor).get()){
                                    autor1 = serviciosAutores.comprobarExistencia(textNombreAutor.getText(), textApellidoAutor.getText()).get();
                                    articulo.setAutor(autor1);
                                    if(serviciosArticulos.addArticulo(articulo).get()){
                                        alertInfo.setContentText("Articulo y autor añadidos");
                                        alertInfo.show();
                                    }else{
                                        alertError.setContentText("Error al añadir el autor");
                                        alertError.show();
                                    }
                                }else{
                                    alertError.setContentText("Error al añadir el autor");
                                    alertError.show();
                                }
                            }
                        } else {
                            alertError.setContentText("Tienes que poner el apellido del autor");
                            alertError.show();
                        }
                    } else {
                        alertError.setContentText("Tienes que poner el nombre del autor");
                        alertError.show();
                    }
                } else {
                    alertError.setContentText("Tienes que elegir un periodico");
                    alertError.show();
                }
            } else {
                alertError.setContentText("Tienes que elegir un tipo");
                alertError.show();
            }
        } else {
            alertError.setContentText("Tienes que poner una descripcion");
            alertError.show();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertError = new Alert(Alert.AlertType.ERROR);
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
    }
}
