package gui.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class InicioController {
    @FXML
    private Label labelBienvenida;

    public void labelBienvenidaText(String nombreUsuario) {
    this.labelBienvenida.setText("Bienvenido/a " + nombreUsuario);
    }
}
