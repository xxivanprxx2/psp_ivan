package gui.controllers;

import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import dao.modelo.Tipo_usuario;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import servicios.ServiciosPeriodicos;
import servicios.ServiciosSuscripciones;
import servicios.ServiciosUsuarios;

import javax.inject.Inject;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class GestionarPeriodicosController implements Initializable {
    @Inject
    private ServiciosPeriodicos serviciosPeriodicos;
    @Inject
    private ServiciosUsuarios serviciosUsuarios;
    @Inject
    private ServiciosSuscripciones serviciosSuscripciones;

    @FXML
    private TextField textNombre;
    @FXML
    private TextField textPrecio;
    @FXML
    private TextField textDirector;
    @FXML
    private TextField textId;
    @FXML
    private TextField textNombreUsuario;
    @FXML
    private TextField textPassword;
    @FXML
    private TextField textPassword2;
    @FXML
    private TextField textMail;
    @FXML
    private ListView<Periodico> listPeriodicos;

    public ListView<Periodico> getListPeriodicos() {
        return listPeriodicos;
    }

    private Alert alertError;
    private Alert alertInfo;
    private Alert alertOption;
    private LoggingController loggingController;
    private boolean idValida = true;
    private Usuario admin = null;

    public Usuario getAdmin() {
        return admin;
    }

    public void setAdmin(Usuario admin) {
        this.admin = admin;
    }

    public void setLoggingController(LoggingController loggingController) {
        this.loggingController = loggingController;
    }

    public void botonAceptar(ActionEvent actionEvent) {
        try {
            int id = (Integer.parseInt(textId.getText()));
            Either<String, Boolean> respuestaIdValida = serviciosPeriodicos.comprobarExistenciaPorId(id);
            respuestaIdValida.peek(opcion -> {
                if (opcion) {
                    idValida = false;
                }
            });
            if (idValida) {
                if (!textNombre.getText().isEmpty() && !textNombre.getText().isBlank()) {
                    if (!textPrecio.getText().isEmpty() && !textPrecio.getText().isBlank()) {
                        try {
                            if (!textDirector.getText().isBlank() && !textDirector.getText().isEmpty()) {
                                if (!textNombreUsuario.getText().isBlank() && !textNombreUsuario.getText().isEmpty()) {
                                    if (!textPassword.getText().isBlank() && !textPassword.getText().isEmpty()) {
                                        if (textPassword.getText().equals(textPassword2.getText())) {
                                            if (!textMail.getText().isBlank() && !textMail.getText().isEmpty()) {
                                                Either<String, Usuario> respuestUsuario = serviciosUsuarios.comprobarUsuario(textNombreUsuario.getText(), textPassword.getText());
                                                respuestUsuario.peek(usuario1 -> {
                                                    setAdmin(usuario1);
                                                });
                                                if (admin == null) {
                                                    Either<String, Usuario> respuestaUsuarioMail = serviciosUsuarios.comprobarUsuarioUser(textMail.getText());
                                                    respuestaUsuarioMail.peek(usuario -> {
                                                        setAdmin(usuario);
                                                    });
                                                    if (admin == null) {
                                                        admin = new Usuario();
                                                        Tipo_usuario tipo_usuario = new Tipo_usuario();
                                                        tipo_usuario.setId(2);
                                                        admin.setUser(textNombre.getText());
                                                        admin.setPassword(textPassword.getText());
                                                        admin.setPrimera_vez(1);
                                                        admin.setTipo_usuario(tipo_usuario);
                                                        String pass = serviciosUsuarios.hashearPass(admin);
                                                        admin.setPassword(pass);
                                                        serviciosUsuarios.addUsuario(admin);
                                                        Either<String, Usuario> respuestaUsuario = serviciosUsuarios.comprobarUsuario(textNombreUsuario.getText(), textPassword.getText());
                                                        respuestaUsuario.peek(usuario1 -> {
                                                            setAdmin(usuario1);
                                                        });
                                                        alertInfo.setContentText("administrador creado");
                                                        alertInfo.show();
                                                    } else {
                                                        alertError.setContentText("El correo ya tiene un usuario");
                                                        alertError.show();
                                                    }
                                                }
                                                try {
                                                    Periodico periodico = new Periodico();
                                                    periodico.setId(id);
                                                    periodico.setNombre(textNombre.getText());
                                                    periodico.setPrecio_diario(Double.parseDouble(textPrecio.getText()));
                                                    periodico.setDirector(textDirector.getText());
                                                    periodico.setUsuario(admin);
                                                    Either<String, Boolean> respuesta = serviciosPeriodicos.addPeriodico(periodico);
                                                    respuesta.peek(opcion -> {
                                                        if (opcion) {
                                                            listPeriodicos.getItems().clear();
                                                            Either<String, List<Periodico>> respuestaPeriodicos = serviciosPeriodicos.devolverTodosPeriodicosAdmin(loggingController.getUser());
                                                            respuestaPeriodicos.peek(periodicos -> {
                                                                listPeriodicos.getItems().addAll(periodicos);
                                                            });
                                                            alertInfo.setContentText("periodico añadido");
                                                            alertInfo.show();
                                                        }
                                                    }).peekLeft(s -> {
                                                        alertError.setContentText("Error al añadir el periodico a la base de datos");
                                                        alertError.show();
                                                    });
                                                }catch (Exception e){
                                                    alertError.setContentText("El precio tiene que ser un numero");
                                                    alertError.show();
                                                }
                                            } else {
                                                alertError.setContentText("Tienes que poner el mail");
                                                alertError.show();
                                            }
                                        } else {
                                            alertError.setContentText("Las contraseñas no son iguales");
                                            alertError.show();
                                        }
                                    } else {
                                        alertError.setContentText("Tienes que poner una contraseña");
                                        alertError.show();
                                    }
                                } else {
                                    alertError.setContentText("Tienes que poner el nombre");
                                    alertError.show();
                                }
                            } else {
                                alertError.setContentText("Tienes que poner el director");
                                alertError.show();
                            }
                        } catch (Exception e) {
                            alertError.setContentText("El precio tiene que ser un numero");
                            alertError.show();
                        }
                    } else {
                        alertError.setContentText("Tienes que poner el precio");
                        alertError.show();
                    }
                } else {
                    alertError.setContentText("Tienes que poner el nombre del periodico");
                    alertError.show();
                }
            } else {
                alertError.setContentText("Ya existe un periodico con esa id para actualizarlo tinees que darle al boton actualizar");
                alertError.show();
            }
        } catch (Exception e) {
            alertError.setContentText("Id tiene que ser un numero");
            alertError.show();
        }
    }

    public void botonBorrar(ActionEvent actionEvent) {
        if (listPeriodicos.getSelectionModel().getSelectedItem() != null) {
            Either<String, List<Suscripcion>> resultado = serviciosSuscripciones.devolverSuscripcionesPeriodico(listPeriodicos.getSelectionModel().getSelectedItem());
            resultado.peek(
                    suscripciones -> {
                        ButtonType opcion = alertOption.showAndWait().orElse(ButtonType.YES);
                        if (ButtonType.YES.equals(opcion)) {
                            Either<String, Boolean> resultadoBorrar = serviciosPeriodicos.borrarPeriodico(listPeriodicos.getSelectionModel().getSelectedItem());
                            resultadoBorrar.peek(o -> {
                                Either<String, List<Periodico>> respuestaPeriodicos = serviciosPeriodicos.devolverTodosPeriodicosAdmin(loggingController.getUser());
                                respuestaPeriodicos.peek(periodicos -> {
                                            listPeriodicos.getItems().addAll(periodicos);
                                            alertInfo.setContentText("Periodico borrado");
                                            alertInfo.show();
                                        }
                                ).peekLeft(string -> {
                                    alertError.setContentText("Error al borrar el periodico");
                                    alertError.show();
                                });
                            });
                        }
                    }).peekLeft(s -> {
                Either<String, Boolean> resultadoBorrar = serviciosPeriodicos.borrarPeriodico(listPeriodicos.getSelectionModel().getSelectedItem());
                resultadoBorrar.peek(o -> {
                            alertInfo.setContentText("Periodico borrado");
                            alertInfo.show();
                        }
                ).peekLeft(string -> {
                    alertError.setContentText("Error al borrar el periodico");
                    alertError.show();
                });
            });
        } else {
            alertError.setContentText("Para borrar tienes que selecionar el periodico");
            alertError.show();
        }

    }

    public void botonActualizar(ActionEvent actionEvent) {
        try {
            int id = (Integer.parseInt(textId.getText()));
            idValida = true;
            Either<String, Boolean> respuestaIdValida = serviciosPeriodicos.comprobarExistenciaPorId(id);
            respuestaIdValida.peek(opcion -> {
                if (opcion) {
                    idValida = false;
                }
            });
            if (!idValida) {
                Periodico periodicoAntiguo = serviciosPeriodicos.devolverPeriodico(id).get();
                Periodico periodicoNuevo = periodicoAntiguo;
                if (!textNombre.getText().isEmpty() && !textNombre.getText().isBlank()) {
                    periodicoNuevo.setNombre(textNombre.getText());
                    if (!textPrecio.getText().isEmpty() && !textPrecio.getText().isBlank()) {
                        try {
                            periodicoNuevo.setPrecio_diario(Double.parseDouble(textPrecio.getText()));
                            if (!textDirector.getText().isBlank() && !textDirector.getText().isEmpty()) {
                                periodicoNuevo.setDirector(textDirector.getText());
                            }
                        } catch (Exception e) {
                            alertError.setContentText("El precio tiene que ser un numero");
                            alertError.show();
                        }
                    }
                }
                if (serviciosPeriodicos.actualizarPeriodico(periodicoNuevo).get()) {
                    alertInfo.setContentText("periodico actualizado");
                    alertInfo.show();
                    listPeriodicos.getItems().clear();
                    Either<String, List<Periodico>> respuestaPeriodicos = serviciosPeriodicos.devolverTodosPeriodicosAdmin(loggingController.getUser());
                    respuestaPeriodicos.peek(periodicos -> {
                        listPeriodicos.getItems().addAll(periodicos);
                    });
                } else {
                    alertError.setContentText("Error al actualizar");
                    alertError.show();
                }
            } else {
                alertError.setContentText("No existe un periodico con esa id, si quieres crearlo debes darle al boton crear");
                alertError.show();
            }
        } catch (Exception e) {
            alertError.setContentText("Id tiene que ser un numero");
            alertError.show();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertError = new Alert(Alert.AlertType.ERROR);
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
        alertOption = new Alert(Alert.AlertType.CONFIRMATION, "Este periodico tiene suscripciones. ¿Estas seguro?", ButtonType.YES, ButtonType.NO);

    }
}
