package gui.controllers;

import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import lombok.SneakyThrows;
import servicios.*;

import javax.inject.Inject;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class LoggingController implements Initializable {
    @Inject
    ServiciosUsuarios serviciosUsuarios;
    @Inject
    ServiciosPeriodicos serviciosPeriodicos;
    @Inject
    ServiciosArticulos serviciosArticulos;
    @Inject
    ServiciosSuscripciones serviciosSuscripciones;
    @Inject
    ServiciosLectores serviciosLectores;


    @FXML
    private TextField textUsuario;
    @FXML
    private PasswordField textPassword;
    @FXML
    private MenuBar menuBar;
    @FXML
    private MenuItem menuGestUsuarios;
    @FXML
    private Menu menuGestPeriodicos;

    @FXML
    private Menu menuSuscripciones;

    private Alert alert;
    private Alert alertInfo;

    @FXML
    private BorderPane bordePaneLogging;

    private Usuario user;
    @Inject
    FXMLLoader fxmlLoaderInicio;
    private InicioController inicioController;
    private AnchorPane panePantallaInicio;

    @Inject
    FXMLLoader fxmlLoaderGestionarCuenta;
    private GestionarCuentaController gestionarCuentaController;
    private AnchorPane paneGestionarCuenta;

    @Inject
    FXMLLoader fxmlLoaderGestionarUsuarios;
    private GestionarUsuariosController gestionarUsuariosController;
    private AnchorPane paneGestionarUsuarios;

    @Inject
    FXMLLoader fxmlLoaderGestionarPeriodicos;
    private GestionarPeriodicosController gestionarPeriodicosController;
    private AnchorPane paneGestionarPeriodicos;

    @Inject
    FXMLLoader fxmlLoaderGestionarArticulos;
    private GestionarArticulosController gestionarArticulosController;
    private AnchorPane paneGestionarArticulos;

    @Inject
    FXMLLoader fxmlLoaderSuscribirse;
    private SuscribirseController suscribirseController;
    private AnchorPane paneSuscribirse;

    @Inject
    FXMLLoader fxmlLoaderSuscripciones;
    private SuscripcionesController suscripcionesController;
    private AnchorPane paneSuscripciones;

    @Inject
    FXMLLoader fxmlLoaderInformes;
    private InformesController informesController;
    private AnchorPane paneInformes;


    @FXML
    private AnchorPane pantallaLogging;

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    @FXML
    private void botonAceptar(ActionEvent actionEvent) {
        Either<String, Usuario> respuesta = serviciosUsuarios.comprobarUsuario(textUsuario.getText(), textPassword.getText());
        respuesta.peek(usuario -> setUser(usuario));
        if (user != null) {
            menuBar.setVisible(true);
            switch (user.getTipo_usuario().getId()) {
                case 1:
                    menuSuscripciones.setVisible(false);
                    menuGestUsuarios.setVisible(true);
                    menuGestPeriodicos.setVisible(true);
                    break;
                case 2:
                    menuGestPeriodicos.setVisible(true);
                    menuGestUsuarios.setVisible(false);
                    menuSuscripciones.setVisible(false);
                    break;
                case 3:
                    menuSuscripciones.setVisible(true);
                    menuGestUsuarios.setVisible(false);
                    menuGestPeriodicos.setVisible(false);
                    break;
            }
            if (user.getPrimera_vez() == 1) {
                cargarGestCuenta();
            } else {
                cargarInicio();
            }
        } else {
            alert.setContentText("ERROR AL INTRODUCIR LOS CAMPOS");
            alert.show();
        }
    }

    @SneakyThrows
    public void cargarInicio() {
        if (user != null) {
            if (panePantallaInicio == null) {
                panePantallaInicio = fxmlLoaderInicio.load(getClass().getResourceAsStream("/fxml/inicio.fxml"));
                inicioController = fxmlLoaderInicio.getController();
            }
            inicioController.labelBienvenidaText(user.getUser());
            bordePaneLogging.setCenter(panePantallaInicio);
        } else {
            alert.setContentText("ERROR AL INTRODUCIR LOS CAMPOS");
            alert.show();
        }
    }

    public void munuBotonGestCuenta(ActionEvent actionEvent) {
        cargarGestCuenta();

    }

    @SneakyThrows
    public void cargarGestCuenta() {
        if (paneGestionarCuenta == null) {
            menuBar.setVisible(true);
            paneGestionarCuenta = fxmlLoaderGestionarCuenta.load(
                    getClass().getResourceAsStream("/fxml/gestionarCuenta.fxml"));
            gestionarCuentaController = fxmlLoaderGestionarCuenta.getController();
            gestionarCuentaController.setLoggingController(this);
        }
        if (user.getTipo_usuario().getId() != 3) {
            gestionarCuentaController.getBotonBorrarCuenta().setVisible(false);
            gestionarCuentaController.getCheckSeguro().setVisible(false);
        }
        if (user.getPrimera_vez() == 1) {
            gestionarCuentaController.getTextNombreNuevo().setDisable(true);
            gestionarCuentaController.getTextCorreoNuevo().setDisable(true);
            alertInfo.setContentText("Es tu primera vez, tienes que cambiar la contraseña");
            alertInfo.show();
        }
        bordePaneLogging.setCenter(paneGestionarCuenta);
    }


    public void menuBotonGestUsuarios(ActionEvent actionEvent) {
        cargarGestUsuarios();
    }

    @SneakyThrows
    public void cargarGestUsuarios() {
        if (user.getPrimera_vez() != 1) {
            if (paneGestionarUsuarios == null) {
                paneGestionarUsuarios = fxmlLoaderGestionarUsuarios.load(
                        getClass().getResourceAsStream("/fxml/gestionarUsuarios.fxml"));
                gestionarUsuariosController = fxmlLoaderGestionarUsuarios.getController();
                gestionarUsuariosController.setLoggingController(this);

            }
            bordePaneLogging.setCenter(paneGestionarUsuarios);
        } else {
            alert.setContentText("Tienes que cambiar la contraseña por ser tu primera vez en la app");
            alert.show();
        }
    }


    public void menuBotonGestPeriodicos(ActionEvent actionEvent) {
        cargarGestPeriodicos();
    }

    @SneakyThrows
    public void cargarGestPeriodicos() {
        if (paneGestionarPeriodicos == null) {
            paneGestionarPeriodicos = fxmlLoaderGestionarPeriodicos.load(
                    getClass().getResourceAsStream("/fxml/gestionarPeriodicos.fxml"));
            gestionarPeriodicosController = fxmlLoaderGestionarPeriodicos.getController();
            gestionarPeriodicosController.setLoggingController(this);
        }
        gestionarPeriodicosController.getListPeriodicos().getItems().clear();
        Either<String, List<Periodico>> respuesta = serviciosPeriodicos.devolverTodosPeriodicosAdmin(user);
        respuesta.peek(periodicos -> {
            gestionarPeriodicosController.getListPeriodicos().getItems().addAll(periodicos);
        });
        bordePaneLogging.setCenter(paneGestionarPeriodicos);
    }


    public void menuBotonGestArticulos(ActionEvent actionEvent) {
        cargarGestArticulos();
    }

    @SneakyThrows
    public void cargarGestArticulos() {
        if (paneGestionarArticulos == null) {
            paneGestionarArticulos = fxmlLoaderGestionarArticulos.load(
                    getClass().getResourceAsStream("/fxml/gestionarArticulos.fxml"));
            gestionarArticulosController = fxmlLoaderGestionarArticulos.getController();
        }
        gestionarArticulosController.getComboTipoArticulo().getItems().clear();
        gestionarArticulosController.getComboPeriodicos().getItems().clear();
        gestionarArticulosController.getComboTipoArticulo().getItems().addAll(serviciosArticulos.devolerTodosTiposArticulos().get());
        List<Periodico> periodicos = serviciosPeriodicos.devolverTodosPeriodicosAdmin(user).get();
        if (periodicos != null) {
            gestionarArticulosController.getComboPeriodicos().getItems().addAll(periodicos);
            bordePaneLogging.setCenter(paneGestionarArticulos);
        } else {
            alert.setContentText("No tienes periodios a los que añadir un articulo");
        }
    }

    public void menuBotonSuscribirse(ActionEvent actionEvent) {
        cargarPantallaSuscribirse();
    }

    @SneakyThrows
    public void cargarPantallaSuscribirse() {
        if (paneSuscribirse == null) {
            paneSuscribirse = fxmlLoaderSuscribirse.load(
                    getClass().getResourceAsStream("/fxml/suscribirse.fxml"));
            suscribirseController = fxmlLoaderSuscribirse.getController();
            suscribirseController.setLoggingController(this);
        }
        suscribirseController.getListPeriodicos().getItems().clear();
        List<Periodico> periodicos = serviciosPeriodicos.devolverTodosPeriodicos().get();
        suscribirseController.getListPeriodicos().getItems().addAll(periodicos);
        bordePaneLogging.setCenter(paneSuscribirse);
    }

    public void menuBotonVerArticulos(ActionEvent actionEvent) {
        cargarPantallaVerArticulos();
    }

    @SneakyThrows
    public void cargarPantallaVerArticulos() {
        if (paneSuscripciones == null) {
            paneSuscripciones = fxmlLoaderSuscripciones.load(
                    getClass().getResourceAsStream("/fxml/suscripciones.fxml"));
            suscripcionesController = fxmlLoaderSuscripciones.getController();
            suscripcionesController.setLoggingController(this);

        }
        Lector lector = serviciosLectores.devolverUnLector(user.getId()).get();
        lector.setId_lector(user.getId());
        suscripcionesController.getListPeriodicos().getItems().clear();
        serviciosSuscripciones.devolverPeriodicosSuscripcionesLector(lector).peek(periodicos -> {
            suscripcionesController.getListPeriodicos().getItems().addAll(periodicos);
        }).peekLeft(s-> {
                alert.setContentText("No tienes suscripciones");
                alert.show();
                });
        bordePaneLogging.setCenter(paneSuscripciones);
    }


    public void menuBotonLogout(ActionEvent actionEvent) {
        cargarpantallaLoggin();
    }

    public void cargarpantallaLoggin() {
        serviciosUsuarios.logout(user);
        bordePaneLogging.setCenter(pantallaLogging);
        menuBar.setVisible(false);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        menuBar.setVisible(false);
        alert = new Alert(Alert.AlertType.ERROR);
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
    }


    public void menuBotonInformes(ActionEvent actionEvent) {
        cargarPantallaInformes();
    }

    @SneakyThrows
    public void cargarPantallaInformes() {
        if (paneInformes == null) {
            paneInformes = fxmlLoaderInformes.load(
                    getClass().getResourceAsStream("/fxml/informes.fxml"));
            informesController = fxmlLoaderInformes.getController();
        }
        bordePaneLogging.setCenter(paneInformes);
    }
}
