package dao.impl;

import dao.DBConnectionPool;
import dao.DaoAutores;
import dao.modelo.ArticuloLeido;
import dao.modelo.Autor;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoAutoresImpl implements DaoAutores {


    @Override
    public Either<String, Autor> comprobarExistencia(String nombre, String apellido) {
        Either<String, Autor> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            Autor a = session.createQuery("select distinct(a) from Autor a " +
                    "where a.nombre = :nombre and a.apellidos = :apellido ", Autor.class).setParameter("nombre", nombre).setParameter("apellido", apellido).getSingleResult();
            resultado = Either.right(a);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }


    @Override
    public Either<String, Boolean> addUsuario(Autor a) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.save(a);
            resultado = Either.right(true);
        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("autor ya existe");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}
