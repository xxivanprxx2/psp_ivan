package dao.impl;

import dao.DBConnectionPool;
import dao.DaoArticulosLeidos;
import dao.DaoSuscripciones;
import dao.DaoUsuariosServer;
import dao.modelo.ArticuloLeido;
import dao.modelo.Lector;
import dao.modelo.Suscripcion;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import utils.HibernateUtils;
import utils.HibernateUtilsSingleton;

import javax.persistence.PersistenceException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoUsuariosServerImpl implements DaoUsuariosServer {


    @Override
    public Either<String, Usuario> getUsuario(String user, String hasPassword) {
        Either<String, Usuario> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            Usuario u = session.createQuery("select distinct(u) from Usuario u " +
                    "where  u.user = :nombre and u.password = :pass ", Usuario.class).setParameter("nombre", user).setParameter("pass", hasPassword).getSingleResult();
            resultado = Either.right(u);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Boolean> actualizarUsuario(Usuario usuario) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtils.getSession()) {
            session.beginTransaction();
            session.update(usuario);
            session.getTransaction().commit();
            resultado = Either.right(true);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }

        return resultado;
    }

    @Override
    public Either<String, Boolean> addUsuario(Usuario usuario) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.save(usuario);
            resultado = Either.right(true);
        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("usuario leido ya existe");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Usuario> getUsuarioUser(String mail) {
        Either<String, Usuario> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            Usuario u = session.createQuery("select distinct(u) from Usuario u " +
                    "where  u.mail = :mail ", Usuario.class).setParameter("mail", mail).getSingleResult();
            resultado = Either.right(u);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, List<Usuario>> devolverTodosLectores() {
        Either<String, List<Usuario>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Usuario> u = session.createQuery("select distinct(u) from Usuario u " +
                    "where  u.tipo_usuario.id = 3", Usuario.class).getResultList();
            resultado = Either.right(u);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Boolean> borrarLector(Usuario usuario) {
        DaoSuscripciones daoSuscripciones = new DaoSuscripcionesImpl();
        DaoArticulosLeidos daoArticulosLeidos = new DaoArticulosLeidosImpl();
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtils.getSession()) {
            Lector lector = Lector.builder()
                    .id_lector(usuario.getId())
                    .build();
            Either<String, List<ArticuloLeido>> articuloLeidos = daoArticulosLeidos.devolverArticulosLeidosLector(lector);
            Either<String, List<Suscripcion>> suscripciones = daoSuscripciones.devolverTodasSuscripcionesUsuario(lector);
            session.beginTransaction();
            articuloLeidos.peek(a -> {
                if (articuloLeidos != null) {
                    for (ArticuloLeido articuloLeido : a) {
                        session.delete(articuloLeido);
                    }
                }
            });
            suscripciones.peek(s -> {
                if (suscripciones != null) {
                    for (Suscripcion suscripcion : s) {
                        session.delete(suscripcion);
                    }
                }
            });
            session.getTransaction().commit();

            resultado = Either.right(true);


        } catch (PersistenceException e) {
            resultado = Either.left("arma tiene datos asociados");
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}