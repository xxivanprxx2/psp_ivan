package dao.impl;

import dao.*;
import dao.modelo.*;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import utils.HibernateUtils;
import utils.HibernateUtilsSingleton;

import javax.persistence.PersistenceException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoPeriodicosImpl implements DaoPeriodicos {


    @Override
    public Either<String, Boolean> borrarPeriodico(Periodico p) {
        DaoArticulos daoArticulos = new DaoArticulosImpl();
        DaoArticulosLeidos daoArticulosLeidos = new DaoArticulosLeidosImpl();
        DaoSuscripciones daoSuscripciones = new DaoSuscripcionesImpl();
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtils.getSession()) {

            session.beginTransaction();
            Either<String, List<ArticuloLeido>> articulosLeidos = daoArticulosLeidos.devolverArticulosLeidos(p);
            articulosLeidos.peek(a -> {
                for (ArticuloLeido articuloLeido : a) {
                    session.delete(articuloLeido);
                }
            });
            Either<String, List<Articulo>> articulos = daoArticulos.devolverTodosArticulosPeriodico(p.getId());
            articulos.peek(a -> {
                for (Articulo art : a) {
                    session.delete(art);
                }
            });
            Either<String, List<Suscripcion>> suscripciones = daoSuscripciones.devolversuscripcionesPeriodico(p);
            suscripciones.peek(s -> {
                for (Suscripcion suscripcion : s) {
                    session.delete(suscripcion);
                }
            });
            session.delete(p);
            session.getTransaction().commit();
            resultado = Either.right(true);

        } catch (PersistenceException e) {
            resultado = Either.left("arma tiene datos asociados");
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Boolean> addPeriodico(Periodico p) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.save(p);
            resultado = Either.right(true);
        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("periodico ya existe");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }


    @Override
    public Either<String, List<Periodico>> getTodosPeriodicosUsuario(Usuario u) {
        Either<String, List<Periodico>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Periodico> p = session.createQuery("select distinct(p) from Periodico p " +
                    "where p.usuario.id = :idUsuario", Periodico.class).setParameter("idUsuario", u.getId()).getResultList();
            resultado = Either.right(p);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, List<Periodico>> getTodosPeriodicos() {
        Either<String, List<Periodico>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Periodico> p = session.createQuery("from Periodico", Periodico.class).getResultList();
            resultado = Either.right(p);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Boolean> comprobarExistenciaporId(int id) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Periodico> p = session.createQuery("select distinct(p) from Periodico p " +
                    "where p.id = :idPeriodico", Periodico.class).setParameter("idPeriodico", id).getResultList();
            if (p == null) {
                resultado = Either.right(true);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Boolean> actualizarPeriodico(Periodico p) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtils.getSession()) {
            session.beginTransaction();
            session.update(p);
            session.getTransaction().commit();
            resultado = Either.right(true);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Periodico> devolverPeriodico(int id) {
        Either<String, Periodico> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            Periodico p = session.createQuery("select distinct(p) from Periodico p " +
                    "where p.id = :idPeriodico", Periodico.class).setParameter("idPeriodico", id).getSingleResult();

            resultado = Either.right(p);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}
