package dao.impl;

import dao.DBConnectionPool;
import dao.DaoInformes;
import dao.modelo.*;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoInformesImpl implements DaoInformes {
    private static String ratingMedioPeriodicos =
            "select  nombre, avg(rating) as rating " +
                    "from articulos_leidos al " +
                    "inner join articulos a on al.id_articulo = a.id " +
                    "inner join periodicos p on a.id_periodico = p.id " +
                    "where a.id_periodico = ?";
    private static String ratingMedioAutor =
            "select nombre, avg(rating) as rating" +
                    "from articulos_leidos al " +
                    "inner join articulos a on al.id_articulo = a.id " +
                    "inner join autores a2 on a.id_autor = a2.id " +
                    "where a2.id = ?";
    private static String listadoAutores =
            "select nombre, avg(rating) as rating " +
                    "from autores au " +
                    "inner join articulos a on a.id_autor = au.id " +
                    "inner join articulos_leidos al on a.id = al.id_articulo " +
                    "group by au.nombre" +
                    "order by mediaRating desc";
    private static String listadoAutoresValor =
            "select * " +
                    "from autores au " +
                    "inner join articulos a on a.id_autor = au.id " +
                    "inner join articulos_leidos al on a.id = al.id_articulo " +
                    "where ? < (select avg(rating)) " +
                    "group by nombre";
    private static String autoresMasRting =
            "select nombre, max(rating) as rating" +
                    "from autores au " +
                    "inner join articulos a on a.id_autor = au.id " +
                    "inner join articulos_leidos al on a.id = al.id_articulo " +
                    "inner join lectores l on al.id_lector = l.id_lector " +
                    "where rating in (select max(rating) " +
                    "from articulos_leidos " +
                    "inner join articulos a2 on articulos_leidos.id_articulo = a2.id " +
                    "inner join lectores l2 on articulos_leidos.id_lector = l2.id_lector " +
                    "where (year(now()) - year(l2.fechaNacimiento) between 31 and 40)) " +
                    "group by au.nombre";
    private static String tipoArticulosOrdenados =
            "select tipo, avg(rating) as  rating " +
                    "from tipos_articulo " +
                    " inner join articulos a on tipos_articulo.id = a.id_tipo " +
                    " inner join articulos_leidos al on a.id = al.id_articulo " +
                    "group by tipo";
    private static String periodicoMasSuscripcionesDia =
            "select nombre, count(id_periodico) as NumeroSuscripciones " +
                    "from periodicos " +
                    "inner join suscripciones s on periodicos.id = s.id_periodico " +
                    "where date(fecha_inicio) = date(?) " +
                    "group by nombre " +
                    "order by NumeroSuscripciones desc " +
                    "limit 1 ";
    private static String periodicoMasGanames =
            "select nombre, precio_diario * count(id_periodico) as total " +
                    "from periodicos " +
                    "inner join suscripciones s on periodicos.id = s.id_periodico " +
                    "where fecha_baja is null " +
                    "group by nombre " +
                    "order by total desc " +
                    "limit 1";
    @Override
    public Either<String, InformeTipo1> ratingMedioPeriodicos(int id) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, InformeTipo1>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(ratingMedioPeriodicos,
                            BeanPropertyRowMapper.newInstance(InformeTipo1.class),
                            new Object[]{id}))
                            .onSuccess(d ->
                                    resultado.set(Either.right(d)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
    @Override
    public Either<String, InformeTipo1> ratingMedioAutor(int id) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, InformeTipo1>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(ratingMedioAutor,
                            BeanPropertyRowMapper.newInstance(InformeTipo1.class),
                            new Object[]{id}))
                            .onSuccess(d ->
                                    resultado.set(Either.right(d)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<InformeTipo1>> listadoAutores() {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<InformeTipo1>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(listadoAutores,
                            BeanPropertyRowMapper.newInstance(InformeTipo1.class)))
                            .onSuccess(autors ->
                                    resultado.set(Either.right(autors)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
    @Override
    public Either<String, List<Autor>> listadoAutoresValor(double valor) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Autor>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(listadoAutoresValor,
                            BeanPropertyRowMapper.newInstance(Autor.class)
                    ,new Object[]{valor}))
                            .onSuccess(autors ->
                                    resultado.set(Either.right(autors)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, InformeTipo1> autoresMasRting() {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, InformeTipo1>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(autoresMasRting,
                            BeanPropertyRowMapper.newInstance(InformeTipo1.class)))
                            .onSuccess(a ->
                                    resultado.set(Either.right(a)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
    @Override
    public Either<String, List<InformeTipo1>> tipoArticulosOrdenados() {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<InformeTipo1>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(tipoArticulosOrdenados,
                            BeanPropertyRowMapper.newInstance(InformeTipo1.class)))
                            .onSuccess(ta ->
                                    resultado.set(Either.right(ta)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, InformesTipo2> periodicoMasSuscripcionesDia(LocalDate date) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, InformesTipo2>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(periodicoMasSuscripcionesDia,
                            BeanPropertyRowMapper.newInstance(InformesTipo2.class)
                            ,new Object[]{date}))
                            .onSuccess(p ->
                                    resultado.set(Either.right(p)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
    @Override
    public Either<String, InformesTipo3> periodicoMasGanames() {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, InformesTipo3>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(periodicoMasGanames,
                            BeanPropertyRowMapper.newInstance(InformesTipo3.class)))
                            .onSuccess(p ->
                                    resultado.set(Either.right(p)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }


}
