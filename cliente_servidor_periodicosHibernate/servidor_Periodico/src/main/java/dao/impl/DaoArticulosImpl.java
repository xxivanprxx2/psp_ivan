package dao.impl;

import dao.DBConnectionPool;
import dao.DaoArticulos;
import dao.modelo.Articulo;
import dao.modelo.Tipo_articulo;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoArticulosImpl implements DaoArticulos {

    @Override
    public Either<String, Boolean> addArticulo(Articulo a) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.save(a);
            resultado = Either.right(true);
        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("articulo ya existe");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, List<Tipo_articulo>> devolverTodosTiposArticulos() {
        Either<String, List<Tipo_articulo>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Tipo_articulo> ta = session.createQuery("from Tipo_articulo", Tipo_articulo.class).getResultList();
            resultado = Either.right(ta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, List<Articulo>> devolverTodosArticulosPeriodico(int id) {
        Either<String, List<Articulo>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Articulo> a = session.createQuery("select distinct(a) from Articulo a " +
                    "where a.periodico.id = :idPeriodico", Articulo.class).setParameter("idPeriodico", id).getResultList();
            resultado = Either.right(a);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}
