package dao.impl;

import dao.DBConnection;
import dao.DBConnectionPool;
import dao.DaoPeriodicos;
import dao.DaoSuscripciones;
import dao.modelo.*;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.HibernateUtils;
import utils.HibernateUtilsSingleton;

import javax.persistence.PersistenceException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoSuscripcionesImpl implements DaoSuscripciones {


    @Override
    public Either<String, Boolean> addSuscripcion(Suscripcion s) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.save(s);
            resultado = Either.right(true);
        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("suscripcion ya existe");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }


    @Override
    public Either<String, List<Periodico>> devolverPeridicosSuscripcionesUsuario(Lector l) {
        Either<String, List<Periodico>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Suscripcion> s = session.createQuery("select distinct(s) from Suscripcion s " +
                    "where s.lectorSuscripcion.id = :idLector", Suscripcion.class).setParameter("idLector", l.getId_lector()).getResultList();
            List<Periodico> p = new ArrayList<>();
            for (Suscripcion suscripcion : s) {
                p.add(suscripcion.getPeriodicoSuscripcion());
            }
            resultado = Either.right(p);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }


    @Override
    public Either<String, List<Suscripcion>> devolverTodasSuscripcionesUsuario(Lector l) {
        Either<String, List<Suscripcion>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Suscripcion> s = session.createQuery("select distinct(s) from Suscripcion s " +
                    "where s.lectorSuscripcion.id = :idLector", Suscripcion.class).setParameter("idLector", l.getId_lector()).getResultList();
            resultado = Either.right(s);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }

    @Override
    public Either<String, List<Suscripcion>> devolverTodasSuscripciones(Periodico p) {
        Either<String, List<Suscripcion>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Suscripcion> s = session.createQuery("from Suscripcion ", Suscripcion.class).getResultList();
            resultado = Either.right(s);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }

    @Override
    public Either<String, Boolean> borrarSuscricion(Usuario usuario, Periodico periodico) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtils.getSession()) {
            Lector lector = Lector.builder()
                    .id_lector(usuario.getId())
                    .build();
            List<Suscripcion> suscripciones = devolverTodasSuscripcionesUsuario(lector).get();
            session.beginTransaction();
            for (Suscripcion suscripcion : suscripciones) {
                session.delete(suscripcion);
            }
            session.getTransaction().commit();

            resultado = Either.right(true);


        } catch (PersistenceException e) {
            resultado = Either.left("arma tiene datos asociados");
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, List<Suscripcion>> devolversuscripcionesPeriodico(Periodico p) {
        Either<String, List<Suscripcion>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<Suscripcion> s = session.createQuery("select distinct(s) from Suscripcion s " +
                    "where s.periodicoSuscripcion.id = :idPeriodico", Suscripcion.class).setParameter("idPeriodico", p.getId()).getResultList();
            resultado = Either.right(s);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}
