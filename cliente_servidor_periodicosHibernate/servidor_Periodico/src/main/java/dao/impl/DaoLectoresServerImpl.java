package dao.impl;

import dao.DBConnectionPool;
import dao.DaoLectoresServer;
import dao.DaoUsuariosServer;
import dao.modelo.ArticuloLeido;
import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.HibernateUtils;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoLectoresServerImpl implements DaoLectoresServer {



    @Override
    public Either<String, Boolean> addLector(Lector l, Usuario u) {
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        Either<String, Boolean> resultado =null;
        AtomicBoolean result = new AtomicBoolean();
        result.set(false);
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            Either<String, Boolean> addUsuario = daoUsuariosServer.addUsuario(u);
            Either<String, Usuario> devolverUsuario = daoUsuariosServer.getUsuario(u.getUser(),u.getPassword());
            addUsuario.peek(o->{
                devolverUsuario.peek(s->{
                    session.save(l);
                    result.set(true);
                });
            });
            resultado = Either.right(result.get());
        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("articulo ya existe");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Lector> devolverLector(long id) {
        Either<String, Lector> resultado =null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            Lector lector = session.createQuery("select distinct(l) from Lector l " +
                    "where l.id = :idLector ", Lector.class).setParameter("idLector", id).getSingleResult();
            resultado = Either.right(lector);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}