package dao.impl;

import dao.DBConnectionPool;
import dao.DaoArticulosLeidos;
import dao.modelo.*;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.HibernateUtils;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoArticulosLeidosImpl implements DaoArticulosLeidos {

    @Override
    public Either<String, Boolean> addArticuloLeido(ArticuloLeido a) {
        Either<String, Boolean> resultado =null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.save(a);
            resultado = Either.right(true);
        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("articulo leido ya existe");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, ArticuloLeido> devolverArticuloLeido(int id_Articulo, Long id_Lector) {
        Either<String, ArticuloLeido> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            ArticuloLeido a = session.createQuery("select distinct(a) from ArticuloLeido a " +
                    "where a.articulo.id = :idArticulo and a.lector.id = :idLector", ArticuloLeido.class).setParameter("idArticulo", id_Articulo).setParameter("idLector", id_Lector).getSingleResult();
            resultado = Either.right(a);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, List<ArticuloLeido>> devolverArticulosLeidos(Periodico p) {
        Either<String, List<ArticuloLeido>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<ArticuloLeido> a = session.createQuery("select distinct(a) from ArticuloLeido a " +
                    "where a.articulo.periodico.id = :idPeriodico", ArticuloLeido.class).setParameter("idPeriodico", p.getId()).getResultList();
            resultado = Either.right(a);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
    @Override
    public Either<String, List<ArticuloLeido>> devolverArticulosLeidosLector(Lector lector) {
        Either<String, List<ArticuloLeido>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            List<ArticuloLeido> a = session.createQuery("select distinct(a) from ArticuloLeido a " +
                    "where a.lector.id = :idLector", ArticuloLeido.class).setParameter("idLector", lector.getId_lector()).getResultList();
            resultado = Either.right(a);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    @Override
    public Either<String, Boolean> actualizarArticuloLeido(ArticuloLeido articuloLeido) {
        Either<String, Boolean> resultado = null;
        try (Session session = HibernateUtils.getSession()) {
            session.beginTransaction();
            session.update(articuloLeido);
            session.getTransaction().commit();
            resultado = Either.right(true);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}
