package dao;

import dao.modelo.ArticuloLeido;
import dao.modelo.Lector;
import dao.modelo.Periodico;
import io.vavr.control.Either;

import java.util.List;

public interface DaoArticulosLeidos {
    Either<String, Boolean> addArticuloLeido(ArticuloLeido a);

    Either<String, ArticuloLeido> devolverArticuloLeido(int id_Articulo, Long id_Lector);

    Either<String, Boolean> actualizarArticuloLeido(ArticuloLeido articuloLeido);

    Either<String, List<ArticuloLeido>> devolverArticulosLeidos(Periodico p);

    Either<String, List<ArticuloLeido>> devolverArticulosLeidosLector(Lector lector);
}
