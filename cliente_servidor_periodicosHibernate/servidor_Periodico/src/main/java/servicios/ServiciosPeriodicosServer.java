package servicios;

import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.util.List;

public interface ServiciosPeriodicosServer {
    Either<String, Boolean> addPeriodico(Periodico periodico);

    Either<String, List<Periodico>> devolverTodosPeriodicosAdmin(Usuario user);

    Either<String, Boolean> comprobarExistenciaPorId(int id);

    Either<String, List<Periodico>> devolverTodosPeriodicos();

    Either<String, Periodico> devolverPeriodico(int id);

    Either<String, Boolean> actualizarPeriodico(Periodico periodico);

    Either<String, Boolean> borrarPeriodico(Periodico periodico);
}
