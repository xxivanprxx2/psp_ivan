package servicios;

import dao.modelo.ArticuloLeido;
import io.vavr.control.Either;

public interface ServiciosArticulosLeidos {
    Either<String, Boolean> addArticulo(ArticuloLeido articuloLeido);

    Either<String, ArticuloLeido> devolverArticuloLeido(int idArticulo, long idLector);

    Either<String, Boolean> actualizarRating(ArticuloLeido articuloLeido);
}
