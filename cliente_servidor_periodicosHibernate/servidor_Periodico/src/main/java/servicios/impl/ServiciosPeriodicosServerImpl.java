package servicios.impl;

import dao.DaoPeriodicos;
import dao.impl.DaoPeriodicosImpl;
import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosPeriodicosServer;

import java.util.List;

public class ServiciosPeriodicosServerImpl implements ServiciosPeriodicosServer {
    @Override
    public Either<String, Boolean> addPeriodico(Periodico periodico) {
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        return daoPeriodicos.addPeriodico(periodico);
    }

    @Override
    public Either<String, List<Periodico>> devolverTodosPeriodicosAdmin(Usuario user) {
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        return daoPeriodicos.getTodosPeriodicosUsuario(user);
    }

    @Override
    public Either<String, Boolean> comprobarExistenciaPorId(int id) {
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        return daoPeriodicos.comprobarExistenciaporId(id);
    }

    @Override
    public Either<String, List<Periodico>> devolverTodosPeriodicos(){
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        return daoPeriodicos.getTodosPeriodicos();
    }

    @Override
    public Either<String, Periodico> devolverPeriodico(int id){
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        return daoPeriodicos.devolverPeriodico(id);
    }

    @Override
    public Either<String, Boolean> actualizarPeriodico(Periodico periodico){
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        return daoPeriodicos.actualizarPeriodico(periodico);
    }
    @Override
    public Either<String, Boolean> borrarPeriodico(Periodico periodico){
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        return daoPeriodicos.borrarPeriodico(periodico);
    }
}
