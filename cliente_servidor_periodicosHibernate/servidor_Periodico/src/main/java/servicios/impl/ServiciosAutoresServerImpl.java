package servicios.impl;

import dao.DaoAutores;
import dao.impl.DaoAutoresImpl;
import dao.modelo.Autor;
import io.vavr.control.Either;
import servicios.ServiciosAutoresServer;

public class ServiciosAutoresServerImpl implements ServiciosAutoresServer {

    @Override
    public Either<String, Autor> comprobarExistencia(String nombre, String apellido){
        DaoAutores daoAutores = new DaoAutoresImpl();
        return daoAutores.comprobarExistencia(nombre,apellido);
    }

    @Override
    public Either<String, Boolean> addAutor(Autor a){
        DaoAutores daoAutores = new DaoAutoresImpl();
        return daoAutores.addUsuario(a);
    }
}
