package servicios.impl;

import dao.DaoArticulosLeidos;
import dao.impl.DaoArticulosLeidosImpl;
import dao.modelo.ArticuloLeido;
import io.vavr.control.Either;
import servicios.ServiciosArticulosLeidos;

public class ServiciosArticulosLeidosIImpl implements ServiciosArticulosLeidos {

    @Override
    public Either<String, Boolean> addArticulo(ArticuloLeido articuloLeido){
        DaoArticulosLeidos daoArticulosLeidos = new DaoArticulosLeidosImpl();
        return  daoArticulosLeidos.addArticuloLeido(articuloLeido);
    }

    @Override
    public Either<String, ArticuloLeido> devolverArticuloLeido(int idArticulo, long idLector){
        DaoArticulosLeidos daoArticulosLeidos = new DaoArticulosLeidosImpl();
        return daoArticulosLeidos.devolverArticuloLeido(idArticulo,idLector);
    }


    @Override
    public Either<String, Boolean> actualizarRating(ArticuloLeido articuloLeido){
        DaoArticulosLeidos daoArticulosLeidos = new DaoArticulosLeidosImpl();
        return daoArticulosLeidos.actualizarArticuloLeido(articuloLeido);
    }
}
