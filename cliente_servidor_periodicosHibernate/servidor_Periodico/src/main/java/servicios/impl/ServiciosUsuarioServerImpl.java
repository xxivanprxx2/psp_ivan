package servicios.impl;

import dao.DaoUsuariosServer;
import dao.impl.DaoUsuariosServerImpl;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import lombok.SneakyThrows;
import servicios.ServiciosUsuarioServer;

import java.util.List;

public class ServiciosUsuarioServerImpl implements ServiciosUsuarioServer {
    @Override
    @SneakyThrows
    public Either<String, Usuario> comprobarUsuarioServer(String usuario, String pass){
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        return daoUsuariosServer.getUsuario(usuario, pass);
    }

    @Override
    public Either<String, Boolean> actualizarUsuario(Usuario usuario){
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        return daoUsuariosServer.actualizarUsuario(usuario);
    }
    @Override
    @SneakyThrows
    public Either<String, Usuario> comprobarUsuarioMail(String mail){
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        return daoUsuariosServer.getUsuarioUser(mail);
    }
    @Override
    @SneakyThrows
    public Either<String, List<Usuario>> devolverTodosLectores(){
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        return daoUsuariosServer.devolverTodosLectores();
    }
    @Override
    public Either<String, Boolean> borrarLector(Usuario usuario){
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        return daoUsuariosServer.borrarLector(usuario);
    }
    @Override
    public Either<String, Boolean> addUsuario(Usuario usuario){
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        return daoUsuariosServer.addUsuario(usuario);
    }
}
