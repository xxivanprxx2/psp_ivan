package servicios.impl;

import dao.DaoInformes;
import dao.impl.DaoInformesImpl;
import dao.modelo.*;
import io.vavr.control.Either;

import java.time.LocalDate;
import java.util.List;

public class ServiciosInformes {

    public Either<String, InformeTipo1> ratingMedioPeriodicos(int id) {
        DaoInformes daoInformes = new DaoInformesImpl();
        return daoInformes.ratingMedioPeriodicos(id);
    }
    public Either<String, InformeTipo1> ratingMedioAutor(int id) {
        DaoInformes daoInformes = new DaoInformesImpl();

        return daoInformes.ratingMedioAutor(id);
    }
    public Either<String, List<InformeTipo1>> listadoAutores() {
        DaoInformes daoInformes = new DaoInformesImpl();

        return daoInformes.listadoAutores();
    }
    public Either<String, List<Autor>> listadoAutoresValor(double valor) {
        DaoInformes daoInformes = new DaoInformesImpl();

        return daoInformes.listadoAutoresValor(valor);
    }
    public Either<String, InformeTipo1> autoresMasRting() {
        DaoInformes daoInformes = new DaoInformesImpl();
        return daoInformes.autoresMasRting();

    }
    public Either<String, List<InformeTipo1>> tipoArticulosOrdenados() {
        DaoInformes daoInformes = new DaoInformesImpl();
        return daoInformes.tipoArticulosOrdenados();
    }
    public Either<String, InformesTipo2> periodicoMasSuscripcionesDia(LocalDate date) {
        DaoInformes daoInformes = new DaoInformesImpl();
        return daoInformes.periodicoMasSuscripcionesDia(date);
    }
    public Either<String, InformesTipo3> periodicoMasGanames() {
        DaoInformes daoInformes = new DaoInformesImpl();
        return daoInformes.periodicoMasGanames();
    }

}
