package servicios.impl;

import dao.DaoLectoresServer;
import dao.impl.DaoLectoresServerImpl;
import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosLectoresServer;

public class ServiciosLectoresServerImpl implements ServiciosLectoresServer {

    @Override
    public Either<String, Boolean> addLector(Lector lector, Usuario usuario){
        DaoLectoresServer daoLectoresServer =  new DaoLectoresServerImpl();
        return daoLectoresServer.addLector(lector, usuario);
    }
    @Override
    public Either<String, Lector> devolverUnLector(long id){
        DaoLectoresServer daoLectoresServer =  new DaoLectoresServerImpl();
        return daoLectoresServer.devolverLector(id);
    }
}
