package EE.servlets;

import dao.modelo.*;
import io.vavr.control.Either;
import servicios.impl.ServiciosInformes;
import utils.Constantes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "ServletInformes", urlPatterns = {"/informe"})
public class ServletInformes extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_RATING_PERIODICOS:
                ratingMedioPeriodicos(request, response);
                break;
            case Constantes.URL_RATING_AUTOR:
                ratingMedioAutor(request, response);
                break;
            case Constantes.URL_LISTAR_AUTORES:
                listadoAutores(request, response);
                break;
            case Constantes.URL_LISTAR_AUTORES_VALOR:
                listadoAutoresValor(request, response);
                break;
            case Constantes.URL_AUTORS_MAS_RATING:
                autoresMasRting(request, response);
                break;
            case Constantes.URL_TIPOS_ARTICULOS_ORDENADOS:
                tipoArticulosOrdenados(request, response);
                break;
            case Constantes.URL_PERIODICO_MAS_DIA:
                periodicoMasSuscripcionesDia(request, response);
                break;
            case Constantes.URL_PERIODICO_MAS_MES:
                periodicoMasGanames(request, response);
                break;
        }
    }

    private void ratingMedioPeriodicos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");
        if (periodico != null) {
            Either<String, InformeTipo1> respuesta = serviciosInformes.ratingMedioPeriodicos(periodico.getId());
            respuesta.peek(a -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, a);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }

    }

    private void ratingMedioAutor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();
        Autor autor = (Autor) request.getAttribute("Autor");
        if (autor != null) {
            Either<String, InformeTipo1> respuesta = serviciosInformes.ratingMedioAutor((int) autor.getId());
            respuesta.peek(a -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, a);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }

    private void listadoAutores(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();
        Either<String, List<InformeTipo1>> respuesta = serviciosInformes.listadoAutores();
        respuesta.peek(a -> {
            request.setAttribute(Constantes.PARAM_RESPUESTA, a);
        }).peekLeft(s -> {
            request.setAttribute(Constantes.PARAM_ERROR, s);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        });
    }

    private void listadoAutoresValor(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();
        try {
            double valor = (double) request.getAttribute("Valor");
            Either<String, List<Autor>> respuesta = serviciosInformes.listadoAutoresValor(valor);
            respuesta.peek(a -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, a);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } catch (Exception e) {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }

    private void autoresMasRting(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();

        Either<String, InformeTipo1> respuesta = serviciosInformes.autoresMasRting();
        respuesta.peek(a -> {
            request.setAttribute(Constantes.PARAM_RESPUESTA, a);
        }).peekLeft(s -> {
            request.setAttribute(Constantes.PARAM_ERROR, s);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        });

    }

    private void tipoArticulosOrdenados(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();

        Either<String, List<InformeTipo1>> respuesta = serviciosInformes.tipoArticulosOrdenados();
        respuesta.peek(a -> {
            request.setAttribute(Constantes.PARAM_RESPUESTA, a);
        }).peekLeft(s -> {
            request.setAttribute(Constantes.PARAM_ERROR, s);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        });

    }

    private void periodicoMasSuscripcionesDia(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();
        try {
            LocalDate date = (LocalDate) request.getAttribute("Valor");
            Either<String, InformesTipo2> respuesta = serviciosInformes.periodicoMasSuscripcionesDia(date);
            respuesta.peek(a -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, a);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        }catch (Exception e){
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }

    private void periodicoMasGanames(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        ServiciosInformes serviciosInformes = new ServiciosInformes();

            Either<String, InformesTipo3> respuesta = serviciosInformes.periodicoMasGanames();
            respuesta.peek(a -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, a);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
    }
}
