package EE.servlets;

import dao.modelo.Articulo;
import dao.modelo.Periodico;
import dao.modelo.Tipo_articulo;
import io.vavr.control.Either;
import servicios.ServiciosArticulosServer;
import servicios.impl.ServiciosArticulosServerImpl;
import utils.Constantes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServletArticulos", urlPatterns = {"/Articulo"})
public class ServletArticulos extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ADD_ARTICULO:
                addArticulo(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_DEVOLVER_TIPOS_ARTICULOS:
                devolverTiposArticulos(request, response);
                break;
            case Constantes.URL_DEVOLVER_ARTICULOS_PERIODICO:
                devolverArticulos(request, response);
                break;
        }
    }

    private void devolverTiposArticulos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosArticulosServer serviciosArticulosServer = new ServiciosArticulosServerImpl();
        Either<String, List<Tipo_articulo>> respuesta = serviciosArticulosServer.devolerTodosTiposArticulos();
        respuesta.peek(tipo_articulos -> {
            request.setAttribute(Constantes.PARAM_RESPUESTA, tipo_articulos);
        }).peekLeft(s -> {
            request.setAttribute(Constantes.PARAM_ERROR, s);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        });
    }
    private void devolverArticulos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosArticulosServer serviciosArticulosServer = new ServiciosArticulosServerImpl();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");
        if (periodico != null) {
            Either<String, List<Articulo>> respuesta = serviciosArticulosServer.devolverArticulosPeriodico(periodico.getId());
            respuesta.peek(a -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, a);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        }else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }
    private void addArticulo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosArticulosServer serviciosArticulosServer = new ServiciosArticulosServerImpl();
        Articulo articulo = (Articulo) request.getAttribute("Articulo");
        if (articulo != null) {
            Either<String, Boolean> respuesta = serviciosArticulosServer.addArticulo(articulo);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, opcion);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }
}
