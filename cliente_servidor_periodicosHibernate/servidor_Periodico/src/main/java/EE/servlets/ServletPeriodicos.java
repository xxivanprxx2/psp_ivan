package EE.servlets;

import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosPeriodicosServer;
import servicios.impl.ServiciosPeriodicosServerImpl;
import utils.Constantes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServletPeriodicos", urlPatterns = {"/Periodico"})
public class ServletPeriodicos extends HttpServlet {


    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ACTUALIZAR_PERIODICO:
                actualizarPeriodico(req, resp);
                break;
            case Constantes.URL_BORRAR_PERIODICO:
                borrarPeriodico(req, resp);
                break;
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ADD_PERIODICO:
                addPeriodico(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_DEVOLVER_PERIODICOS_USER:
                devolverPeriodicosUser(request, response);
                break;
            case Constantes.URL_COMPROBAR_ID_PERIODICO:
                comprobarId(request, response);
                break;
            case Constantes.URL_DEVOLVER_PERIODICO:
                devolverPeriodico(request, response);
            case Constantes.URL_DEVOLVER_PERIODICOS:
                devolverPeriodicos(request, response);
        }
    }

    private void actualizarPeriodico(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosPeriodicosServer serviciosPeriodicosServer = new ServiciosPeriodicosServerImpl();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");
        if (periodico != null) {
            Either<String, Boolean> respuesta = serviciosPeriodicosServer.actualizarPeriodico(periodico);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, true);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Periodico no valido");
        }
    }
    private void borrarPeriodico(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosPeriodicosServer serviciosPeriodicosServer = new ServiciosPeriodicosServerImpl();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");
        if (periodico != null) {
            Either<String, Boolean> respuesta = serviciosPeriodicosServer.borrarPeriodico(periodico);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, true);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Usuario no valido");
        }
    }
    private void devolverPeriodicosUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosPeriodicosServer serviciosPeriodicosServer = new ServiciosPeriodicosServerImpl();
        Usuario user = (Usuario) request.getAttribute("Usuario");

        if (user != null) {
            Either<String, List<Periodico>> respuesta = serviciosPeriodicosServer.devolverTodosPeriodicosAdmin(user);
            respuesta.peek(periodicos -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, periodicos);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }

    private void devolverPeriodicos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosPeriodicosServer serviciosPeriodicosServer = new ServiciosPeriodicosServerImpl();
        Either<String, List<Periodico>> respuesta = serviciosPeriodicosServer.devolverTodosPeriodicos();
        respuesta.peek(periodicos -> {
            request.setAttribute(Constantes.PARAM_RESPUESTA, periodicos);
        }).peekLeft(s -> {
            request.setAttribute(Constantes.PARAM_ERROR, s);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        });
    }

    private void devolverPeriodico(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosPeriodicosServer serviciosPeriodicosServer = new ServiciosPeriodicosServerImpl();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");

        if (periodico != null) {
            Either<String, Periodico> respuesta = serviciosPeriodicosServer.devolverPeriodico(periodico.getId());
            respuesta.peek(periodicos -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, periodicos);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }

    }

    private void addPeriodico(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosPeriodicosServer serviciosPeriodicosServer = new ServiciosPeriodicosServerImpl();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");
        if (periodico != null) {
            Either<String, Boolean> respuesta = serviciosPeriodicosServer.addPeriodico(periodico);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, opcion);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }

    private void comprobarId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosPeriodicosServer serviciosPeriodicosServer = new ServiciosPeriodicosServerImpl();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");
        if (periodico != null) {
            Either<String, Boolean> respuesta = serviciosPeriodicosServer.comprobarExistenciaPorId(periodico.getId());
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, opcion);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }
}
