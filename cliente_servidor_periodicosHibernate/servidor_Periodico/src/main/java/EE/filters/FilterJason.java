package EE.filters;

import com.google.gson.Gson;
import dao.modelo.*;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import utils.Constantes;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
@WebFilter(filterName = "FilterJson", urlPatterns = {"/Usuario","/Periodico","/ArticuloLeido","/Suscripcion","/Autor","/Articulo","/informe"})
public class FilterJason implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        AtomicBoolean seguir = new AtomicBoolean(true);
        Gson gson = new Gson();
        Optional.ofNullable(req.getParameter(Constantes.PARAM_USUARIO))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, Usuario.class))
                            .onSuccess(usuario ->
                                    req.setAttribute(Constantes.PARAM_USUARIO, usuario))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        Optional.ofNullable(req.getParameter(Constantes.PARAM_LECTOR))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, Lector.class))
                            .onSuccess(lector ->
                                    req.setAttribute(Constantes.PARAM_LECTOR, lector))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        Optional.ofNullable(req.getParameter(Constantes.PARAM_PERIODICO))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, Periodico.class))
                            .onSuccess(periodico ->
                                    req.setAttribute(Constantes.PARAM_PERIODICO, periodico))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        Optional.ofNullable(req.getParameter(Constantes.PARAM_ARTICULO))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, Articulo.class))
                            .onSuccess(articulo ->
                                    req.setAttribute(Constantes.PARAM_ARTICULO, articulo))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        Optional.ofNullable(req.getParameter(Constantes.PARAM_AUTOR))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, Autor.class))
                            .onSuccess(autor ->
                                    req.setAttribute(Constantes.PARAM_AUTOR, autor))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        Optional.ofNullable(req.getParameter(Constantes.PARAM_SUSCRIPCION))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, Suscripcion.class))
                            .onSuccess(suscripcion ->
                                    req.setAttribute(Constantes.PARAM_SUSCRIPCION, suscripcion))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        Optional.ofNullable(req.getParameter(Constantes.PARAM_ARTICULO_LEIDO))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, ArticuloLeido.class))
                            .onSuccess(articulo_leido ->
                                    req.setAttribute(Constantes.PARAM_ARTICULO_LEIDO, articulo_leido))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        Optional.ofNullable(req.getParameter(Constantes.PARAM_VALOR))
                .ifPresent(o -> {
                    Try.of(() -> gson.fromJson(o, double.class))
                            .onSuccess(valor ->
                                    req.setAttribute(Constantes.PARAM_VALOR, valor))
                            .onFailure(throwable -> {
                                log.error(throwable.getMessage(), throwable);
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_BAD_REQUEST);
                                seguir.set(false);
                            });
                });
        AtomicReference<String> respuesta = new AtomicReference<>();
        if (seguir.get()) {
            chain.doFilter(req, resp);

            Optional.ofNullable(
                    req.getAttribute(Constantes.PARAM_RESPUESTA))
                    .ifPresentOrElse(
                            o -> respuesta.set(gson.toJson(o))
                            , () -> {
                                respuesta.set(
                                        Optional.ofNullable(
                                                req.getAttribute(Constantes.PARAM_ERROR))
                                                .map(o -> (String)o)
                                                .orElseGet(() -> null));
                                ((HttpServletResponse) resp).setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                            });
        }
        resp.getWriter().print(respuesta.get());
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
