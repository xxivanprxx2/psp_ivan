/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import lombok.Getter;
import lombok.Setter;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author dam2
 */
@Getter
@Setter
public class ConfigurationServer {

    private static ConfigurationServer config;

    private ConfigurationServer() {

    }


    public static ConfigurationServer cargarInstance(InputStream file) {

        if (config == null) {
            try {
                Yaml yaml = new Yaml();
                config = yaml.loadAs(file,
                        ConfigurationServer.class);
            } catch (Exception ex) {
                Logger.getLogger(ConfigurationServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return config;
    }

    public static ConfigurationServer getInstance() {

        return config;
    }

    private String driverDB;
    private String ruta;
    private String user;
    private String password;
}
