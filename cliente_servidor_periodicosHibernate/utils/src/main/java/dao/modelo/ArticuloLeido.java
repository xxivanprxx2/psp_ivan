package dao.modelo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(name="articulos_leidos")
public class ArticuloLeido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column
    private int rating;
    @ManyToOne
    @JoinColumn (name="id_lector",referencedColumnName = "id_lector",nullable = false)
    private Lector lector;
    @ManyToOne
    @JoinColumn (name="id_articulo",referencedColumnName = "id",nullable = false)
    private Articulo articulo;
}
