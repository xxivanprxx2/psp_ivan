package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(name="articulos")
public class Articulo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column
    private String titular;
    @Basic
    @Column
    private String descripcion;
    @ManyToOne
    @JoinColumn (name="id_periodico",referencedColumnName = "id",nullable = false)
    private Periodico periodico;
    @ManyToOne
    @JoinColumn (name="id_tipo",referencedColumnName = "id",nullable = false)
    private Tipo_articulo tipo_articulo;
    @ManyToOne
    @JoinColumn (name="id_autor",referencedColumnName = "id",nullable = false)
    private Autor autor;

    @OneToMany( mappedBy = "articulo")
    private List<ArticuloLeido> articulosLeidos;

    public Articulo(String titular, String descripcion, Periodico periodico, Tipo_articulo tipo_articulo, Autor autor) {
        this.titular = titular;
        this.descripcion = descripcion;
        this.periodico = periodico;
        this.tipo_articulo = tipo_articulo;
        this.autor = autor;
    }
}
