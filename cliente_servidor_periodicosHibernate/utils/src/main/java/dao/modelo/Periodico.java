package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(name= "periodicos")
public class Periodico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(name = "nombre")
    private String nombre;
    @Basic
    @Column
    private Double precio_diario;
    @Basic
    @Column
    private String director;
    @ManyToOne
    @JoinColumn (name="id_administrador",referencedColumnName = "id",nullable = false)
    private Usuario usuario;

    @OneToMany( mappedBy = "periodico")
    private List<Articulo> articulos;

    @OneToMany( mappedBy = "periodicoSuscripcion")
    private List<Suscripcion> suscripciones;
}
