package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(name= "suscripciones")
public class Suscripcion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column
    private LocalDate fecha_inicio;
    @Basic
    @Column
    private LocalDate fecha_baja;
    @ManyToOne
    @JoinColumn (name="id_lector",referencedColumnName = "id",nullable = false)
    private Usuario lectorSuscripcion;
    @ManyToOne
    @JoinColumn (name="id_periodico",referencedColumnName = "id",nullable = false)
    private Periodico periodicoSuscripcion;
}
