package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(name="lectores")
public class Lector extends Usuario{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_lector;
    @Basic
    @Column
    private String nombre;
    @Basic
    @Column
    private LocalDate fechaNacimiento;
    @OneToMany( mappedBy = "lector")
    private List<ArticuloLeido> articulosLeidos;



}
