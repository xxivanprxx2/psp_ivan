package dao.modelo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name= "tipos_usuario")
public class Tipo_usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column
    private String tipo;
    @Basic
    @Column
    private String descripcion;
    @OneToMany( mappedBy = "tipo_usuario")
    private List<Usuario> usuarios;

}
