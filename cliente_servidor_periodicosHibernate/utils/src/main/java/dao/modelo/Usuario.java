package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Table(name= "usuarios")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column
    private String user;

    @Basic
    @Column
    private String password;
    @Basic
    @Column
    private int primera_vez;

    @Basic
    @Column
    private String mail;

    @ManyToOne
    @JoinColumn (name="id_tipo_usuario",referencedColumnName = "id_tipo_usuario",nullable = false)
    private Tipo_usuario tipo_usuario;

    @OneToMany( mappedBy = "usuario")
    private List<Periodico> periodicos;

    @OneToMany( mappedBy = "lectorSuscripcion")
    private List<Suscripcion> suscripciones;

}
