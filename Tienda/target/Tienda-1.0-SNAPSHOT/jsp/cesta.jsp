<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xxiva
  Date: 29/10/2020
  Time: 20:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cesta</title>
    <link rel="stylesheet" href="bootstrap-4.5.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-4.5.3-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-4.5.3-dist/css/bootstrap-reboot.min.css">

</head>
<body>
<h1>Esta es tu cesta</h1>
<form action="cesta">
    <table class="table table-dark">
        <c:forEach var="item" items="${numList}">
            <tr>
                <c:forEach var="i" begin="0" end="1" step="1" varStatus="status">
                    <td>
                        <c:choose>
                            <c:when test="${i ==0 }">${item}</c:when>
                        </c:choose>
                    </td>
                </c:forEach>
            </tr>
        </c:forEach>
    </table>
    <input class="btn btn-primary" type="submit" name="op" value="limpiar"/>
    <input class="btn btn-secondary" type="submit"  name="op" value="logout"/>
    <input class="btn btn-secondary" type="submit"  name="op" value="volver"/>
</form>
</body>
</html>
