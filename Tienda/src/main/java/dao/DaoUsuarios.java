package dao;

import dao.modelo.Tipo_usuario;
import dao.modelo.Usuario;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
@Log4j2
public class DaoUsuarios {

    private static final String SELECT_UN_USUARIO =
            "select * from usuarios u inner join tipos_usuario tu on u.id_tipo_usuario = tu.id_tipo_usuario " + " where u.user = ? and u.password = ?";

    public Usuario getUsuario(String user, String hasPassword) {
        DBConnection db = new DBConnection();
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        Usuario usuario = null;
        try {
            connection = db.getConnection();
            stmt = connection.prepareStatement(SELECT_UN_USUARIO);
            stmt.setString(1, user);
            stmt.setString(2, hasPassword);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                usuario = new Usuario(
                        resultSet.getInt("u.id"),
                        resultSet.getString("user"),
                        resultSet.getString("password"),
                        resultSet.getInt("primera_vez"),
                        resultSet.getString("mail"),
                        new Tipo_usuario(
                                resultSet.getInt("tu.id_tipo_usuario"),
                                resultSet.getString("tipo"),
                                resultSet.getString("descripcion")));
            }
        } catch (Exception e) {
            log.error(e);
        } finally {
            db.cerrarResultSet(resultSet);
            db.cerrarStatement(stmt);
            db.cerrarConexion(connection);
        }
        return usuario;
    }
}
