package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class Usuario {
    private long id;
    private String user;
    private String password;
    private int primera_vez;
    private String mail;
    private Tipo_usuario tipo_usuario;

    public Usuario(String user, String password, int primera_vez, String mail, Tipo_usuario tipo_usuario) {
        this.user = user;
        this.password = password;
        this.primera_vez = primera_vez;
        this.mail = mail;
        this.tipo_usuario = tipo_usuario;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", primera_vez=" + primera_vez +
                ", mail='" + mail + '\'' +
                ", tipo_usuario=" + tipo_usuario +
                '}';
    }
}
