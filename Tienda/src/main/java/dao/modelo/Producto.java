package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class Producto {
    private int id;
    private String nombre;
    private double precio;

    @Override
    public String toString() {
        return nombre;
    }
}
