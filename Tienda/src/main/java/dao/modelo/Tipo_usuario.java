package dao.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Tipo_usuario {
    private int id;
    private final String tipo;
    private final String descripcion;

    public Tipo_usuario(int id, String tipo, String descripcion) {
        this.id = id;
        this.tipo = tipo;
        this.descripcion = descripcion;
    }
}
