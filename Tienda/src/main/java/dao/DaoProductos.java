package dao;

import dao.modelo.Producto;
import dao.modelo.Tipo_usuario;
import dao.modelo.Usuario;
import lombok.extern.log4j.Log4j2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DaoProductos {
    private static String SELECT_TODOS_PRODUCTOS =
            "select * from productos";

    public List<String> getTodosProductos() {
        DBConnection db = new DBConnection();
        Connection connection = null;
        PreparedStatement pst = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        List<String> productos = new ArrayList<>();
        try {
            connection = db.getConnection();
            stmt = connection.createStatement();
            pst = connection.prepareStatement(SELECT_TODOS_PRODUCTOS);
            resultSet = pst.executeQuery();
            while (resultSet.next()) {
                Producto producto = new Producto(
                        resultSet.getInt("id"),
                        resultSet.getString("nombre"),
                        resultSet.getDouble("precio"));

                productos.add(producto.getNombre());
            }
        } catch (Exception e) {
            log.error(e);
        } finally {
            db.cerrarResultSet(resultSet);
            db.cerrarStatement(stmt);
            db.cerrarConexion(connection);
        }
        return productos;
    }
}
