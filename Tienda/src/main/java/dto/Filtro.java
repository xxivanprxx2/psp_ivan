package dto;


import servicios.ServiciosLogging;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;



@WebFilter(filterName = "FilterLogin",urlPatterns = {"/productos"})
public class Filtro implements Filter {
    public void destroy() {

    }

    public void init(FilterConfig config) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;

            if (request.getSession().getAttribute("usuario") == null){
                filterChain.doFilter(request, servletResponse);
            }else{
                request.getRequestDispatcher("jsp/error.jsp").forward(request,servletResponse);

            }

    }
}
