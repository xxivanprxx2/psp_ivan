package servicios;

import dao.DaoUsuarios;
import dao.modelo.Usuario;
import lombok.SneakyThrows;

import java.security.MessageDigest;

public class ServiciosLogging {
    @SneakyThrows
    public Usuario comprobarUsuario(String usuario, String pass){
        DaoUsuarios daoUsuarios=new DaoUsuarios();
        MessageDigest md = MessageDigest.getInstance("SHA3-512");
        byte[] hashBytes = md.digest(pass.getBytes());
        var contraseñaHashed = bytesToHex(hashBytes);
        return daoUsuarios.getUsuario(usuario, contraseñaHashed);
    }
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
