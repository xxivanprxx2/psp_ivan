package servicios;

import dao.DaoProductos;
import dao.modelo.Producto;

import java.util.List;

public class ServiciosProductos {

    public List<String> devolverProductos(){
        DaoProductos daoProductos = new DaoProductos();
        return daoProductos.getTodosProductos();
    }
}
