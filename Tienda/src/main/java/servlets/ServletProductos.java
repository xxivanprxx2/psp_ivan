package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ServletCesta",  urlPatterns = {"/producto"})
public class ServletProductos extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        add(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        add(request, response);

    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> cesta = (List) request.getSession().getAttribute("cesta");
        List<String> productosAñadir = new ArrayList<>();
        if (request.getParameterValues("producto") != null) {
            String producto= request.getParameter("producto");
            String[] productos = producto.split(",");
            productosAñadir.addAll(Arrays.asList(productos));
        }
        if (!productosAñadir.contains("[]")) {
            if (cesta == null) {
                cesta = new ArrayList<>();
                cesta.addAll(productosAñadir);
            } else {
                if (cesta.contains("(CESTA_VACIA)")){
                    cesta.remove(0);
                }
                for(String s : productosAñadir){
                    cesta.add(s);
                }
            }
        }else if(cesta == null){
            cesta = new ArrayList<>();
            cesta.add("(CESTA_VACIA)");
        }
        request.getSession().setAttribute("cesta", cesta);
        response.getWriter().print(request.getSession().getAttribute("cesta"));
    }
}
