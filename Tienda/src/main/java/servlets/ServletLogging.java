package servlets;

import dao.DaoProductos;
import dto.Filtro;
import servicios.ServiciosLogging;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletProductos", urlPatterns = {"/logging"})

public class ServletLogging extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        cargarProductos(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        cargarProductos(request, response);
    }

    private void cargarProductos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosLogging serviciosLogging = new ServiciosLogging();
        request.getSession().setAttribute("usuario", request.getParameter("usuario"));

        DaoProductos daoProductos = new DaoProductos();
            if(serviciosLogging.comprobarUsuario(request.getParameter("usuario"), request.getParameter("password")) != null){
                response.getWriter().print(daoProductos.getTodosProductos());

            }else {
                response.getWriter().print("No valido");
            }
        request.setAttribute("numList", daoProductos.getTodosProductos());
      //request.getRequestDispatcher("jsp/productos.jsp").forward(request, response);
    }
}
