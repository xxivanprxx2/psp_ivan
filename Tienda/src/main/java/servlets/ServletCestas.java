package servlets;

import dao.DaoProductos;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@WebServlet(name = "ServletCestas", urlPatterns = {"/cesta"})
public class ServletCestas extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        cesta(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        cesta(request, response);
    }

    private void cesta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        var operador = (String) request.getParameter("op");
        switch (operador) {
            case "limpiar":
                comprarCesta(request, response);
                break;
            case "volver":
                volver(request, response);
                break;
        }
    }
    private void comprarCesta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> cesta = new ArrayList<>();
        if (request.getSession().getAttribute("cesta") != null) {
            request.getSession().setAttribute("cesta", cesta);
            cesta.add("(CESTA_VACIA)");
            request.setAttribute(("numList"), cesta);
        }
        response.getWriter().print(cesta);
    }

    private void volver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> cesta = (List) request.getSession().getAttribute("cesta");
        DaoProductos daoProductos = new DaoProductos();
        List<String> productos = daoProductos.getTodosProductos();
        int sizeTabla = productos.size();
        if (cesta != null) {
            for (int i = 0; i < sizeTabla; i++) {
                if(sizeTabla > productos.size()){
                    i--;
                    sizeTabla = productos.size();
                }
                for (int j = 0; j < cesta.size(); j++) {
                    String producto = cesta.get(j).replace("[","");
                    producto = producto.replace("]","");
                    producto = producto.replace(" ","");
                    cesta.set(j, producto);
                    if (productos.get(i).equals(cesta.get(j))) {
                        productos.remove(i);
                    }
                }
            }
        }
        response.getWriter().print(productos);
    }

}
