package gui;

import config.ConfigurationServer;
import config.ConfigurationSingleton_OkHttpClient;
import okhttp3.OkHttpClient;

import javax.enterprise.inject.Produces;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;


public class Producers {


    @Produces
    public ConfigurationServer createConfiguration() {

        return ConfigurationServer.getInstance();
    }
    @Produces
    public OkHttpClient createOkHttpClient() {

        return ConfigurationSingleton_OkHttpClient.getInstance();
    }

    @Produces
    public Validator createValidator()
    {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return validator;
    }

}
