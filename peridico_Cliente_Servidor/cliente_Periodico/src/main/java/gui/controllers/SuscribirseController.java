package gui.controllers;

import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import servicios.ServiciosLectores;
import servicios.ServiciosSuscripciones;

import javax.inject.Inject;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class SuscribirseController implements Initializable {
    @Inject
    private ServiciosSuscripciones serviciosSuscripciones;
    @Inject
    private ServiciosLectores serviciosLectores;

    @FXML
    private ListView<Periodico> listPeriodicos;


    private Alert alertError;
    private Alert alertInfo;
    private LoggingController loggingController;
    private Date fecha_inicio;
    private Date fecha_fin;


    public ListView<Periodico> getListPeriodicos() {
        return listPeriodicos;
    }

    public void setLoggingController(LoggingController loggingController) {
        this.loggingController = loggingController;
    }

    public void botonAceptar(ActionEvent actionEvent) {
        if (listPeriodicos.getSelectionModel().getSelectedItem() != null) {
            Lector lector = serviciosLectores.devolverUnLector(loggingController.getUser().getId()).get();
            lector.setId(loggingController.getUser().getId());
            Suscripcion suscripcion = new Suscripcion(
                    (int) lector.getId(),
                    listPeriodicos.getSelectionModel().getSelectedItem().getId(),
                    fecha_inicio,
                    fecha_fin
            );
            List<Suscripcion> suscripciones = new ArrayList<>();
                    serviciosSuscripciones.devolverSuscripcionesLector(lector).peek(s ->{
                                suscripciones.addAll(s);
                            });
            boolean suscrito = false;
            if (!suscripciones.isEmpty()) {
                for (Suscripcion s : suscripciones) {
                    if (s.getId_periodico() == suscripcion.getId_periodico()) {
                        suscrito = true;
                    }
                }
            }
            if (!suscrito) {
                Either<String, Boolean> respuesta = serviciosSuscripciones.addSuscripcion(suscripcion);
                respuesta.peek(opcion -> {
                    alertInfo.setContentText("Suscrito");
                    alertInfo.show();
                }).peekLeft(s-> {
                    alertError.setContentText("Error al suscribir");
                    alertError.show();
                });
            } else {
               serviciosSuscripciones.borrarSuscripcion(loggingController.getUser(), listPeriodicos.getSelectionModel().getSelectedItem());
               Either<String, Boolean> respuesta = serviciosSuscripciones.addSuscripcion(suscripcion);
                respuesta.peek(opcion -> {
                    alertInfo.setContentText("Suscrito");
                    alertInfo.show();
                }).peekLeft(s-> {
                    alertError.setContentText("Error al suscribir");
                    alertError.show();
                });
            }
        } else {
            alertError.setContentText("Tienes que selecionar un periodico");
            alertError.show();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertError = new Alert(Alert.AlertType.ERROR);
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
        LocalDate localDate = LocalDate.now();
        fecha_inicio = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        LocalDate localDate2 = localDate.plusYears(1);
        fecha_fin = Date.from(localDate2.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

    }
}
