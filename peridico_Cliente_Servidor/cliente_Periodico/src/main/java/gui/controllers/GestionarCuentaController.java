package gui.controllers;

import dao.modelo.Usuario;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import servicios.ServiciosUsuarios;
import servicios.ServiciosLectores;

import javax.inject.Inject;
import java.net.URL;
import java.util.ResourceBundle;

public class GestionarCuentaController implements Initializable {
    @Inject
    ServiciosUsuarios serviciosUsuarios;

    @Inject
    ServiciosLectores serviciosLectores;

    @FXML
    private TextField textNombreNuevo;
    @FXML
    private TextField textCorreoNuevo;
    @FXML
    private TextField textPassNueva;
    @FXML
    private TextField textPassNueva2;
    @FXML
    private CheckBox checkSeguro;
    @FXML
    private Button botonBorrarCuenta;

    private boolean añadido = false;

    public void setAñadido(boolean añadido) {
        this.añadido = añadido;
    }

    public TextField getTextNombreNuevo() {
        return textNombreNuevo;
    }

    public TextField getTextCorreoNuevo() {
        return textCorreoNuevo;
    }

    private Alert alertError;
    private Alert alertInfo;

    private LoggingController loggingController;

    public void setLoggingController(LoggingController loggingController) {
        this.loggingController = loggingController;
    }

    public Button getBotonBorrarCuenta() {
        return botonBorrarCuenta;
    }

    public CheckBox getCheckSeguro() {
        return checkSeguro;
    }

    public void botonAceptar(ActionEvent actionEvent) {
        Usuario usuarioNuevo = new Usuario(
                loggingController.getUser().getUser(),
                loggingController.getUser().getPassword(),
                loggingController.getUser().getPrimera_vez(),
                loggingController.getUser().getMail(),
                loggingController.getUser().getId_tipo_usuario());
        usuarioNuevo.setId(loggingController.getUser().getId());
        if (!textNombreNuevo.getText().isBlank() && !textNombreNuevo.getText().isEmpty()) {
            usuarioNuevo.setUser(textNombreNuevo.getText());

        }
        if (!textCorreoNuevo.getText().isBlank() && !textCorreoNuevo.getText().isEmpty()) {
            usuarioNuevo.setMail(textCorreoNuevo.getText());
        }
        if (!textPassNueva.getText().isBlank() && !textPassNueva.getText().isEmpty()) {
            if (!textPassNueva2.getText().isBlank() && !textPassNueva2.getText().isEmpty()) {
                if (textPassNueva.getText().equals(textPassNueva2.getText())) {
                    usuarioNuevo.setPassword(textPassNueva.getText());
                    String newPass = serviciosUsuarios.hashearPass(usuarioNuevo);
                    usuarioNuevo.setPassword(newPass);
                } else {
                    alertError.setContentText("Contraseñas no son iguales");
                    alertError.show();
                }
            }
        } else if (usuarioNuevo.getPrimera_vez() == 1) {
            alertError.setContentText("Tienes que cambiar la contraseña por ser tu primera vez en la aapp");
            alertError.show();
        }
        if (loggingController.getUser() != usuarioNuevo) {
            usuarioNuevo.setPrimera_vez(0);
            Either<String, Boolean> respuesta = serviciosUsuarios.actualizarUsuario(usuarioNuevo);
            respuesta.peek(opcion -> setAñadido(opcion));
            if (!añadido) {
                alertError.setContentText("error al actualizar usuario");
            } else {
                loggingController.setUser(usuarioNuevo);
                alertInfo.setContentText("Usuario actualizado");
                if (loggingController.getUser().getPrimera_vez() == 0) {
                    textNombreNuevo.setDisable(false);
                    textCorreoNuevo.setDisable(false);
                }
                alertInfo.show();
            }
        }
    }

    public void botonBorrarCuenta(ActionEvent actionEvent) {
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertError = new Alert(Alert.AlertType.ERROR);
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
    }
}
