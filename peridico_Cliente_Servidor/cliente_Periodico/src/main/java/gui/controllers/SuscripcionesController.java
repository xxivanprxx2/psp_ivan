package gui.controllers;

import dao.modelo.Articulo;
import dao.modelo.ArticuloLeido;
import dao.modelo.Lector;
import dao.modelo.Periodico;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import servicios.ServiciosArticulos;
import servicios.ServiciosArticulosLeidos;
import servicios.ServiciosLectores;
import servicios.ServiciosSuscripciones;

import javax.inject.Inject;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SuscripcionesController implements Initializable {
    @Inject
    private ServiciosArticulos serviciosArticulos;
    @Inject
    private ServiciosLectores serviciosLectores;
    @Inject
    private ServiciosArticulosLeidos serviciosArticulosLeidos;
    @Inject
    private ServiciosSuscripciones serviciosSuscripciones;
    @FXML
    private ListView<Periodico> listPeriodicos;
    @FXML
    private ComboBox<Articulo> comboArticulos;
    @FXML
    private TextArea textArticulo;
    @FXML
    private TextField textPuntuacion;

    private LoggingController loggingController;
    private Alert alertError;
    private Alert alertInfo;

    public ListView<Periodico> getListPeriodicos() {
        return listPeriodicos;
    }

    public void setLoggingController(LoggingController loggingController) {
        this.loggingController = loggingController;
    }

    public void botonAceptarPeriodico(ActionEvent actionEvent) {
        if (listPeriodicos.getSelectionModel().getSelectedItem() != null) {
            comboArticulos.getItems().clear();
            List<Articulo> articulos = new ArrayList<>();
            serviciosArticulos.devolverArticulosPeriodico(listPeriodicos.getSelectionModel().getSelectedItem().getId()).
                    peek(a -> {
                        articulos.addAll(a);
                    }).peekLeft(s -> {
                alertError.setContentText("No tiene articulos");
                alertError.show();
            });
            if (!articulos.isEmpty()) {
                comboArticulos.getItems().addAll(articulos);
                alertInfo.setContentText("articulos cargados");
                alertInfo.show();
            }
        } else {
            alertError.setContentText("tienes que selecionar un periodico");
            alertError.show();
        }
    }

    public void botonAceptarArticulo(ActionEvent actionEvent) {
        if (listPeriodicos.getSelectionModel().getSelectedItem() != null) {
            if (comboArticulos.getSelectionModel().getSelectedItem() != null) {
                textArticulo.setText(comboArticulos.getSelectionModel().getSelectedItem().getDescripcion());
            } else {
                alertError.setContentText("tienes que selecionar un articulo");
                alertError.show();
            }
        } else {
            alertError.setContentText("tienes que selecionar un periodico");
            alertError.show();
        }
    }

    public void botonPuntuar(ActionEvent actionEvent) {
        if (!textArticulo.getText().isEmpty() && !textArticulo.getText().isBlank()) {
            if (!textPuntuacion.getText().isEmpty() && !textPuntuacion.getText().isBlank()) {
                try {
                    int puntuacion = Integer.parseInt(textPuntuacion.getText());
                    Articulo articulo = comboArticulos.getValue();
                    Lector lector = serviciosLectores.devolverUnLector(loggingController.getUser().getId()).get();
                    lector.setId(loggingController.getUser().getId());
                    ArticuloLeido articuloLeido = new ArticuloLeido(
                            (int) lector.getId_lector(),
                            articulo.getId(),
                            puntuacion);
                    ArticuloLeido articuloLeidoAntiguo = new ArticuloLeido(0,0,0,0);
                    Either<String, ArticuloLeido> respuestaArticuloLeido = serviciosArticulosLeidos.devolverArticuloLeido(articulo.getId(), lector.getId());
                    respuestaArticuloLeido.peek(a -> {
                        articuloLeidoAntiguo.setId(a.getId());
                        articuloLeidoAntiguo.setId_articulo(a.getId_articulo());
                        articuloLeidoAntiguo.setId_lector(a.getId_lector());
                        articuloLeidoAntiguo.setRating(a.getRating());
                    });
                    if (articuloLeidoAntiguo.getId() != 0) {
                        if (serviciosArticulosLeidos.actualizarRating(Integer.parseInt(textPuntuacion.getText()), articuloLeidoAntiguo.getId()).get()) {
                            alertInfo.setContentText("Puntuacion articulo actualiza");
                            alertInfo.show();
                        } else {
                            alertError.setContentText("No se ha podido actualizar");
                            alertError.show();
                        }
                    } else {
                        Either<String, Boolean> opcion = serviciosArticulosLeidos.addArticulo(articuloLeido);
                        opcion.peek(o -> {
                            alertInfo.setContentText("Articulo puntuado");
                            alertInfo.show();
                        }).peekLeft(s -> {
                            alertError.setContentText("Error al actualizar la puntacion");
                            alertError.show();
                        });
                    }
                } catch (Exception e) {
                    alertError.setContentText("La puntuacion tiene que ser un numero natural");
                    alertError.show();
                }
            }
        } else {
            alertError.setContentText("para puntuar tinees que tener un articulo abierto");
            alertError.show();
        }
    }

    public void botonBorrarSuscripcion(ActionEvent actionEvent) {
        if (listPeriodicos.getSelectionModel().getSelectedItem() != null) {
            if(serviciosSuscripciones.borrarSuscripcion(loggingController.getUser(), listPeriodicos.getSelectionModel().getSelectedItem()).get()) {
                Lector lector = serviciosLectores.devolverUnLector(loggingController.getUser().getId()).get();
                lector.setId_lector(loggingController.getUser().getId());
                listPeriodicos.getItems().clear();
                serviciosSuscripciones.devolverPeriodicosSuscripcionesLector(lector).peek(periodicos -> {
                    getListPeriodicos().getItems().addAll(periodicos);
                }).peekLeft(s-> {
                    alertError.setContentText("No tienes suscripciones");
                    alertError.show();
                });                alertInfo.setContentText("Suscripcion borrada");
                alertInfo.show();
            }else{
                alertError.setContentText("error al desuscribir");
                alertError.show();
            }
        } else {
            alertError.setContentText("tienes que selecionar un periodico");
            alertError.show();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alertError = new Alert(Alert.AlertType.ERROR);
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
        textArticulo.setEditable(false);
    }

}
