package gui.controllers;

import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import servicios.ServiciosLectores;
import servicios.ServiciosUsuarios;

import javax.inject.Inject;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class GestionarUsuariosController implements Initializable {
    @Inject
    private ServiciosUsuarios serviciosUsuarios;
    @Inject
    private ServiciosLectores serviciosLectores;
    @FXML
    private TextField textNombre;
    @FXML
    private TextField textCorreo;
    @FXML
    private TextField textPass;
    @FXML
    private TextField textPass2;
    @FXML
    private DatePicker dateFechaNacimiento;
    @FXML
    private ListView<Usuario> listUsuarios;

    private boolean mailValido = true;
    private Alert alertError;
    private Alert alertInfo;
    LoggingController loggingController;
    private Usuario usuarioNuevo;

    public void setLoggingController(LoggingController loggingController) {
        this.loggingController = loggingController;
    }

    public void botonAceptar(ActionEvent actionEvent) {
        if (!textNombre.getText().isEmpty() && !textNombre.getText().isBlank()) {
            if (!textCorreo.getText().isEmpty() && !textCorreo.getText().isBlank()) {
                if (!textPass.getText().isBlank() && !textPass.getText().isEmpty()) {
                    if (!textPass.getText().isBlank() && !textPass.getText().isEmpty()) {
                        if (textPass.getText().equals(textPass2.getText())) {
                            if (dateFechaNacimiento.getValue() != null) {
                                usuarioNuevo = new Usuario(
                                        textNombre.getText(),
                                        textPass.getText(),
                                        1,
                                        textCorreo.getText(),
                                        3);
                                String pass = serviciosUsuarios.hashearPass(usuarioNuevo);
                                usuarioNuevo.setPassword(pass);
                                Either<String, Usuario> respuestMailValido = serviciosUsuarios.comprobarUsuarioUser(textCorreo.getText());
                                respuestMailValido.peek(user -> {
                                    if (user != null) {
                                        mailValido = false;
                                    }
                                });
                                if (mailValido) {
                                    Lector lector = new Lector(
                                            usuarioNuevo.getUser(),
                                            usuarioNuevo.getPassword(),
                                            1,
                                            usuarioNuevo.getMail(),
                                            usuarioNuevo.getId_tipo_usuario(),
                                            usuarioNuevo.getUser(),
                                            dateFechaNacimiento.getValue());
                                    Either<String, Boolean> respuesta = serviciosLectores.addLector(lector, usuarioNuevo);
                                    respuesta.peek(opcion -> {
                                        if (opcion) {
                                            alertInfo.setContentText("usuario añadidido");
                                            alertInfo.show();
                                        }
                                    });
                                } else {
                                    alertError.setContentText("Este correo ya tiene un usuario");
                                    alertError.show();
                                }
                            } else {
                                alertError.setContentText("Tienes que selecionar fecha");
                                alertError.show();
                            }
                        } else {
                            alertError.setContentText("Las contraseñas no son iguales");
                            alertError.show();
                        }
                    } else {
                        alertError.setContentText("Tienes que comprobar la contraseña");
                        alertError.show();
                    }
                } else {
                    alertError.setContentText("Tienes que poner la contraseña");
                    alertError.show();
                }
            } else {
                alertError.setContentText("Tienes que poner el correo");
                alertError.show();
            }
        } else {
            alertError.setContentText("Tienes que poner el nombre");
            alertError.show();
        }
        listUsuarios.getItems().clear();
        Either<String, List<Usuario>> respuesta = serviciosUsuarios.devolverTodosusuarios();
        respuesta.peek(usuarios -> {
            listUsuarios.getItems().addAll(usuarios);
        });
    }

    public void botonBorrar(ActionEvent actionEvent) {
        if (listUsuarios.getSelectionModel().getSelectedItem() != null) {
            Usuario usuario = listUsuarios.getSelectionModel().getSelectedItem();
            Either<String, Boolean> respuesta = serviciosUsuarios.borrarUsuario(usuario);
            respuesta.peek(opcion -> {
                if (opcion) {
                    alertInfo.setContentText("Usuario borrado");
                    alertInfo.show();
                }
            }).peekLeft(s -> {
                alertError.setContentText("Error al borrar");
                alertError.show();
            });
            listUsuarios.getItems().clear();
            Either<String, List<Usuario>> respuest = serviciosUsuarios.devolverTodosusuarios();
            respuest.peek(usuarios -> {
                listUsuarios.getItems().addAll(usuarios);
            });
        } else {
            alertError.setContentText("Para borrar tienes que seleccionar un usuario");
            alertError.show();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Either<String, List<Usuario>> respuest = serviciosUsuarios.devolverTodosusuarios();
        respuest.peek(usuarios -> {
            listUsuarios.getItems().addAll(usuarios);
        });
        alertError = new Alert(Alert.AlertType.ERROR);
        alertInfo = new Alert(Alert.AlertType.INFORMATION);
    }
}
