package gui.controllers;

import dao.modelo.*;
import io.vavr.control.Either;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.springframework.cglib.core.Local;
import servicios.ServiciosInformes;


import javax.inject.Inject;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class InformesController implements Initializable {
    @Inject
    ServiciosInformes serviciosInformes;
    @FXML
    private ComboBox<String> comboOpcion;
    @FXML
    private TextArea textResultado;
    @FXML
    private TextField textValor;
    @FXML
    private TextField textId;
    @FXML
    private DatePicker datePicker;

    private Alert alertError;

    public void botonAceptar(ActionEvent actionEvent) {
        textResultado.clear();
        String opcion = comboOpcion.getSelectionModel().getSelectedItem();
        switch (opcion) {
            case "El rating medio de todos los articulos de un periodico":

                if (!textId.getText().isEmpty()) {
                    try {
                        int id = Integer.parseInt(textId.getText());
                        Either<String, InformeTipo1> resultado = serviciosInformes.ratingMedioPeriodicos(id);
                        resultado.peek(d -> {
                            textResultado.clear();
                            textResultado.setText("" + d);
                        });
                    } catch (Exception e) {
                        alertError.setContentText("la id tiene que ser un numero");
                        alertError.show();
                    }
                } else {
                    alertError.setContentText("tienes que poner la id");
                    alertError.show();
                }
                break;
            case "El rating medio de todos los articulos de un autor":
                if (!textId.getText().isEmpty()) {
                    try {
                        int id = Integer.parseInt(textId.getText());
                        Either<String, InformeTipo1> resultado2 = serviciosInformes.ratingMedioAutor(id);
                        resultado2.peek(d -> {
                            textResultado.clear();
                            textResultado.setText("" + d);
                        });
                    } catch (Exception e) {
                        alertError.setContentText("la id tiene que ser un numero");
                        alertError.show();
                    }
                } else {
                    alertError.setContentText("tienes que poner la id");
                    alertError.show();
                }
                break;
            case "Un listado de todos los autores ordenado por el rating":
                Either<String, List<InformeTipo1>> resultado3 = serviciosInformes.listadoAutores();
                resultado3.peek(autores -> {
                    textResultado.clear();
                    for (InformeTipo1 a : autores) {
                        textResultado.setText("" + a);
                    }
                });
                break;
            case "Un listado de ciertos autores dados siempre que su rating medio sea mayor a un valor dado":

                if (!textValor.getText().isEmpty()) {
                    try {
                        double valor = Integer.parseInt(textValor.getText());
                        Either<String, List<Autor>> resultado4 = serviciosInformes.listadoAutoresValor(valor);
                        resultado4.peek(autores -> {
                            textResultado.clear();
                            for (Autor a : autores) {
                                textResultado.setText("" + a);
                            }
                        });
                        textResultado.setText("" + serviciosInformes.listadoAutoresValor(valor));
                    } catch (Exception e) {
                        alertError.setContentText("El valor tiene que ser un numero");
                        alertError.show();
                    }
                } else {
                    alertError.setContentText("tienes que poner valor");
                    alertError.show();
                }
                break;
            case "El autor con mas rating para los lectores que tengan entre 31 y 40 años":
                Either<String, InformeTipo1> resultado5 = serviciosInformes.autoresMasRting();
                resultado5.peek(autor -> {
                    textResultado.clear();
                    textResultado.setText("" + autor);
                });
                break;
            case "Los tipos de articulos ordenados segun el rating de los lectores":
                Either<String, List<InformeTipo1>> resultado6 = serviciosInformes.tipoArticulosOrdenados();
                resultado6.peek(i -> {
                    textResultado.clear();
                    for (InformeTipo1 in : i) {
                        textResultado.setText("" + in);
                    }
                });
                break;
            case "El periodico con mas suscripciones a un día determinado":
                if (!textValor.getText().isEmpty()) {
                    try {
                        LocalDate date = datePicker.getValue();
                        Either<String, InformesTipo2> resultado7 = serviciosInformes.periodicoMasSuscripcionesDia(date);
                        resultado7.peek(p -> {
                            textResultado.clear();
                            textResultado.setText("" + p);

                        });
                    } catch (Exception e) {
                        alertError.setContentText("El dia tiene que ser un numero");
                        alertError.show();
                    }
                } else {
                    alertError.setContentText("tienes que poner dia");
                    alertError.show();
                }
                break;
            case "El periodico que más gana en un mes determinado":

                Either<String, InformesTipo3> resultado8 = serviciosInformes.periodicoMasGanames();
                resultado8.peek(p -> {
                    textResultado.clear();
                    textResultado.setText("" + p);

                });
                break;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        comboOpcion.getItems().addAll("El rating medio de todos los articulos de un periodico"
                , "El rating medio de todos los articulos de un autor"
                , "Un listado de todos los autores ordenado por el rating"
                , "Un listado de ciertos autores dados siempre que su rating medio sea mayor a un valor dado"
                , "El autor con mas rating para los lectores que tengan entre 31 y 40 años"
                , "Los tipos de articulos ordenados segun el rating de los lectores"
                , "El periodico con mas suscripciones a un día determinado"
                , "El periodico que más gana en un mes determinado");
        alertError = new Alert(Alert.AlertType.ERROR);
    }
}
