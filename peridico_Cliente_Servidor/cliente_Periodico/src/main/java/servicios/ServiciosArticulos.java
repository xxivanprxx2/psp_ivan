package servicios;

import dao.modelo.Articulo;
import dao.modelo.Tipo_articulo;
import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.util.List;

public interface ServiciosArticulos {
    Either<String, List<Tipo_articulo>> devolerTodosTiposArticulos();

    Either<String, Boolean> addArticulo(Articulo articulo);

    Either<String, List<Articulo>> devolverArticulosPeriodico(int id);
}
