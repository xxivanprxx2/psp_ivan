package servicios.impl;

import dao.DaoSuscripciones;
import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosSuscripciones;

import javax.inject.Inject;
import java.util.List;

public class ServiciosSuscripcionesImpl implements ServiciosSuscripciones {
    @Inject
    DaoSuscripciones daoSuscripciones;
    @Override
    public Either<String, Boolean> addSuscripcion(Suscripcion suscripcion){
        return daoSuscripciones.addSuscripcion(suscripcion);
    }

    @Override
    public Either<String, List<Periodico>> devolverPeriodicosSuscripcionesLector(Lector lector){
        return daoSuscripciones.devolverPeridicosSuscripcionesUsuario(lector);
    }
    @Override
    public Either<String, List<Suscripcion>> devolverSuscripcionesLector(Lector lector){
        return daoSuscripciones.devolverTodasSuscripcionesUsuario(lector);
    }
    @Override
    public Either<String, List<Suscripcion>> devolverSuscripcionesPeriodico(Periodico periodico){
        return daoSuscripciones.devolverTodasSuscripciones(periodico);
    }

    @Override
    public Either<String, Boolean> borrarSuscripcion(Usuario u, Periodico p){
        return daoSuscripciones.borrarSuscricion(u,p);
    }
}
