package dao;

import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.util.List;

public interface DaoPeriodicos {
    Either<String, Boolean> borrarPeriodico(Periodico periodico);

    Either<String, Boolean> addPeriodico(Periodico periodico);

    Either<String, List<Periodico>> getTodosPeriodicosUsuario(Usuario u);

    Either<String, List<Periodico>> getTodosPeriodicos();

    Either<String, Boolean> comprobarExistenciaporId(int id);

    Either<String, Boolean> actualizarPeriodico(Periodico periodico);

    Either<String, Periodico> devolverPeriodico(int id);
}
