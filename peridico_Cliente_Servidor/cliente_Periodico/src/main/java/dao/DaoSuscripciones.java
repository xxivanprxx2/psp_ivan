package dao;

import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.util.List;

public interface DaoSuscripciones {
    Either<String, Boolean> addSuscripcion(Suscripcion s);

    Either<String, List<Periodico>> devolverPeridicosSuscripcionesUsuario(Lector l);

    Either<String, List<Suscripcion>> devolverTodasSuscripcionesUsuario(Lector l);

    Either<String, List<Suscripcion>> devolverTodasSuscripciones(Periodico p);

    Either<String, Boolean> borrarSuscricion(Usuario u, Periodico p);
}
