package dao;

import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;

public interface DaoLectores {

    Either<String, Boolean> addLector(Lector l, Usuario u);

    Either<String, Lector> devolverLector(long id);
}
