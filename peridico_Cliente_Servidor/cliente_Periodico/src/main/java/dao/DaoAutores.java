package dao;

import dao.modelo.Autor;
import io.vavr.control.Either;

public interface DaoAutores {
    Either<String, Autor> comprobarExistencia(String nombre, String apellido);

    Either<String, Boolean> addAutor(Autor autor);
}
