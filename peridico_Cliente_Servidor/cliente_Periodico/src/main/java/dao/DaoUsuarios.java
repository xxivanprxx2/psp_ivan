package dao;

import dao.modelo.Usuario;
import io.vavr.control.Either;

import java.util.List;

public interface DaoUsuarios {
    Either<String, Usuario> getUsuario(String user, String hasPassword);

    Either<String, Usuario> getUsuarioUser(String user);

    Either<String, Boolean> actualizarUsuario(Usuario usuario);

    Either<String,List<Usuario>> getTodosUsuarios();

    Either<String, Boolean> borrarUsuario(Usuario usuario);

    Either<String, Boolean> addUsuario(Usuario u);

    Either<String, Boolean> logOut(Usuario u);
}
