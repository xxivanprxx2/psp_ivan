package dao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import config.ConfigurationServer;
import dao.impl.DBConnectionPoolImpl;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;

public interface DBConnectionPool {
    static DBConnectionPoolImpl getInstance() {
        if (DBConnectionPoolImpl.dbconection == null) {
            DBConnectionPoolImpl.dbconection = new DBConnectionPoolImpl();
        }

        return DBConnectionPoolImpl.dbconection;
    }

    Connection getConnection() throws Exception;

    DataSource getDataSourceFromServer() throws NamingException;

    default HikariDataSource getDataSourceHikari() {
        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(ConfigurationServer.getInstance().getRuta());
        config.setUsername(ConfigurationServer.getInstance().getUser());
        config.setPassword(ConfigurationServer.getInstance().getPassword());
        config.setDriverClassName(ConfigurationServer.getInstance().getDriverDB());
        config.setMaximumPoolSize(2);

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        HikariDataSource datasource = new HikariDataSource(config);

        return datasource;
    }

    DataSource getDataSource();

    void cerrarConexion(Connection connection);

    void cerrarPool();
}
