package dao.impl;

import dao.DBConnectionPool;
import dao.DaoArticulos;
import dao.modelo.Articulo;
import dao.modelo.Tipo_articulo;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoArticulosImpl implements DaoArticulos {
    private static String SELECT_TODOS_TIPOS_ARTICULOS =
            "select * from tipos_articulo";
    private static String SELECT_TODOS_ARTICULOS_USUARIO =
            "select * from articulos a " +
                    "inner join tipos_articulo ta on a.id_tipo = ta.id " +
                    "inner join autores au on a.id_autor = au.id " +
                    "inner join periodicos p on a.id_periodico = p.id " +
                    "where p.id_administrador = ?  ";
    private static String INSECTAR_ARTICULO =
            "insert into articulos  (titular,descripcion,id_periodico,id_tipo,id_autor) " +
                    "values(?,?,?,?,?) ";
    private static String DEVOLVER_ARTICULO_ID=
            "select * from articulos a  " +
                    "inner join tipos_articulo ta on a.id_tipo = ta.id " +
                    "inner join autores au on a.id_autor = au.id " +
                    "inner join periodicos p on a.id_periodico = p.id " +
                    "where a.id = ?";
    private static String DEVOLVER_ARTICULOS_PERIODICO=
            "select * from articulos a " +
                    "where a.id_periodico = ?";
    @Override
    public Either<String, Boolean> addArticulo(Articulo a) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(INSECTAR_ARTICULO,
                            new Object[]{a.getTitular(), a.getDescripcion(), a.getId_periodico(), a.getId_tipo(), a.getId_autor()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }

    @Override
    public Either<String, List<Tipo_articulo>> devolverTodosTiposArticulos() {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Tipo_articulo>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(SELECT_TODOS_TIPOS_ARTICULOS,
                            BeanPropertyRowMapper.newInstance(Tipo_articulo.class)))
                            .onSuccess(tipo_articulos ->
                                    resultado.set(Either.right(tipo_articulos)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable ->{
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);

        });
        return resultado.get();
    }

   @Override
    public Either<String, List<Articulo>> devolverTodosArticulosPeriodico(int id){
       DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
       AtomicReference<Either<String, List<Articulo>>> resultado = new AtomicReference<>();
       Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
               .onSuccess(jtm -> {
                   Try.of(() -> jtm.query(DEVOLVER_ARTICULOS_PERIODICO,
                           BeanPropertyRowMapper.newInstance(Articulo.class),
                           new Object[]{id}))
                           .onSuccess(articulos ->
                                   resultado.set(Either.right(articulos)))
                           .onFailure(throwable -> {
                               resultado.set(Either.left(throwable.getMessage()));
                               log.error(throwable.getMessage(), throwable);
                           });
               }).onFailure(throwable ->{
           resultado.set(Either.left(throwable.getMessage()));
           log.error(throwable.getMessage(), throwable);
       });
       return resultado.get();    }


}
