package dao.impl;

import dao.DBConnectionPool;
import dao.DaoArticulosLeidos;
import dao.modelo.ArticuloLeido;
import dao.modelo.Periodico;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
@Log4j2
public class DaoArticulosLeidosImpl implements DaoArticulosLeidos {
    private static String INSERTAR_ARTICULO_LEIDO =
            "insert into articulos_leidos (id_lector,id_articulo,rating) " +
                    "values(?,?,?) ";
    private static String ACTUALIZAR_PUNTUACION =
            "update articulos_leidos al set al.rating = ? where al.id = ?";
    private static String DEVOLVER_ARTICULO_LEIDO =
            "select * from articulos_leidos al " +
                    "where al.id_articulo = ? and al.id_lector = ?";

    @Override
    public Either<String, Boolean> addArticuloLeido(ArticuloLeido a) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(INSERTAR_ARTICULO_LEIDO,
                            new Object[]{a.getId_lector(),a.getId_articulo(),a.getRating()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });

        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }

    @Override
    public Either<String, ArticuloLeido> devolverArticuloLeido(int id_Articulo, Long id_Lector) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, ArticuloLeido>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(DEVOLVER_ARTICULO_LEIDO,
                            BeanPropertyRowMapper.newInstance(ArticuloLeido.class),
                            new Object[]{id_Articulo, id_Lector}))
                            .onSuccess(periodico ->
                                    resultado.set(Either.right(periodico)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable ->{
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> actualizarArticuloLeido(int rating, int id) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(ACTUALIZAR_PUNTUACION,
                            new Object[]{rating,id}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }
}
