package dao.impl;

import dao.DBConnectionPool;
import dao.DaoUsuariosServer;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoUsuariosServerImpl implements DaoUsuariosServer {
    private static final String SELECT_UN_USUARIO =
            "select * from usuarios u " +
                    " where u.user = ? and u.password = ?";
    private static String ACTUALIZAR_USUARIO =
            "update usuarios u set u.password = ?, u.primera_vez = ?, u.user = ?, u.mail = ? " +
                    "where u.id = ?";
    private static String QUERY_INSERTAR_USUARIO =
            "insert into usuarios  (user,password,primera_vez,mail,id_tipo_usuario) " +
                    "values(?,?,true,?,?) ";

    private static final String SELECT_UN_USUARIO_MAIL =
            "select * from usuarios u " +
                    " where u.mail = ?";
    private static String SELECT_TODOS_LECTORES =
            "select * from usuarios u " +
                    "where u.id_tipo_usuario = 3";
    private static String BORRAR_ARTICULO_LEIDO =
            "delete from articulos_leidos a where a.id = ? ";
    private static String BORRAR_SUSCRIPCION =
            "delete from suscripciones s where s.id = ? ";
    private static String BORRAR_LECTOR =
            "delete from lectores l where l.id_lector = ? ";
    private static String BORRAR_USUARIO =
            "delete from usuarios u where u.id = ? ";

    @Override
    public Either<String, Usuario> getUsuario(String user, String hasPassword) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Usuario>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(SELECT_UN_USUARIO,
                            BeanPropertyRowMapper.newInstance(Usuario.class),
                            new Object[]{user, hasPassword}))
                            .onSuccess(usuario ->
                                    resultado.set(Either.right(usuario)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });

        return resultado.get();
    }

    @Override
    public Either<String, Boolean> actualizarUsuario(Usuario usuario) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(ACTUALIZAR_USUARIO,
                            new Object[]{usuario.getPassword(), usuario.getPrimera_vez(), usuario.getUser(), usuario.getMail(), usuario.getId()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> addUsuario(Usuario usuario) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(QUERY_INSERTAR_USUARIO,
                            new Object[]{usuario.getUser(), usuario.getPassword(), usuario.getMail(), usuario.getId_tipo_usuario()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }

    @Override
    public Either<String, Usuario> getUsuarioUser(String mail) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Usuario>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(SELECT_UN_USUARIO_MAIL,
                            BeanPropertyRowMapper.newInstance(Usuario.class),
                            new Object[]{mail}))
                            .onSuccess(usuario ->
                                    resultado.set(Either.right(usuario)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);

                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);

                                }
                            });
                }).onFailure(throwable ->{
                resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });

        return resultado.get();
    }

    @Override
    public Either<String, List<Usuario>> devolverTodosLectores() {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Usuario>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(SELECT_TODOS_LECTORES,
                            BeanPropertyRowMapper.newInstance(Usuario.class)))
                            .onSuccess(usuario ->
                                    resultado.set(Either.right(usuario)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable ->{
                resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);

        });
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> borrarLector(Usuario usuario) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(
                dbConnectionPool.getDataSource());
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        int resultSetInt = 0;
        try {
            JdbcTemplate jtm = new JdbcTemplate(
                    transactionManager.getDataSource());

            jtm.update(BORRAR_ARTICULO_LEIDO, usuario.getId());

            jtm.update(BORRAR_SUSCRIPCION, usuario.getId());

            jtm.update(BORRAR_LECTOR, usuario.getId());

            resultSetInt = jtm.update(BORRAR_USUARIO, usuario.getId());
            transactionManager.commit(txStatus);
        } catch (Exception e) {
            log.error("Error" + e);
        }
        if (resultSetInt == 1) {
            resultado.set(Either.right(true));
        } else {
            resultado.set(Either.left("Error al borrar"));
        }
        return resultado.get();
    }
}