package dao.impl;

import config.ConfigurationServer;
import dao.DBConnection;
import dao.DBConnectionPool;
import lombok.extern.log4j.Log4j2;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


@Log4j2
public class DBConnectionImpl implements DBConnection {

    public DBConnectionImpl() {

    }


    @Override
    public Connection getConnection() throws Exception {

        Connection connection = null;

        // solo hace falta en web.
        Class.forName(ConfigurationServer.getInstance().getDriverDB());

        connection = DBConnectionPool.getInstance().getConnection();
        return connection;
    }

    @Override
    public DataSource getDataSource()  {
        return DBConnectionPool.getInstance().getDataSource();
    }

    @Override
    public void cerrarPool()
    {
        DBConnectionPool.getInstance().cerrarPool();
    }

    @Override
    public void cerrarConexion(Connection connection) {
        try {
            if (connection != null) {
                connection.setAutoCommit(true);
                connection.close();
            }
        } catch (SQLException ex) {

            log.error("no se ha podido cerrar conexion", ex);
        }
    }
    @Override
    public void cerrarStatement(Statement stmt) {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException ex) {
            log.error("", ex);
        }
    }
    @Override
    public void cerrarResultSet(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException ex) {
            log.error("", ex);
        }
    }
    @Override
    public void rollbackCon(Connection con) {
        try {
            if (con != null)
                con.rollback();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


}
