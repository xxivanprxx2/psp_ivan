package dao.impl;

import dao.DBConnection;
import dao.DBConnectionPool;
import dao.DaoArticulos;
import dao.DaoPeriodicos;
import dao.modelo.Articulo;
import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.sql.Statement;
import java.time.Period;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoPeriodicosImpl implements DaoPeriodicos {
    private static String SELECT_TODOS_PERIODICOS_USUARIO =
            "select * from periodicos p " +
                    "where p.id_administrador= ?";
    private static String INSERTAR_PERIODICO =
            "insert into periodicos  (id,nombre,precio_diario,director,id_administrador) " +
                    "values(?,?,?,?,?) ";
    private static String PERIODICO_POR_ID =
            "select * from periodicos p " +
                    "where p.id = ?";
    private static String ACTUALIZAR_PERIODICO =
            "update periodicos p set p.nombre = ?, p.precio_diario = ?, p.director = ? " +
                    "where p.id = ?";
    private static String SELECT_TODOS_PERIODICOS =
            "select * from periodicos p ";
    private static String BORRAR_ARTICULO_LEIDO =
            "delete from articulos_leidos al " +
                    "where al.id_articulo = ? ";
    private static String BORRAR_ARTICULO =
            "delete from articulos a where a.id_periodico = ? ";
    private static String BORRAR_SUSCRIPCION =
            "delete from suscripciones s where s.id_periodico = ? ";
    private static String BORRAR_PERIODICO =
            "delete from periodicos p where p.id = ? ";

    @Override
    public Either<String, Boolean> borrarPeriodico(Periodico periodico) {
        DaoArticulos daoArticulos = new DaoArticulosImpl();
        DBConnection db = new DBConnectionImpl();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(
                db.getDataSource());
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        int resultSetInt = 0;
        try {
            JdbcTemplate jtm = new JdbcTemplate(
                    transactionManager.getDataSource());
            Either<String, List<Articulo>> articulos = daoArticulos.devolverTodosArticulosPeriodico(periodico.getId());
                articulos.peek(a->{
                        for (Articulo art : a) {
                            jtm.update(BORRAR_ARTICULO_LEIDO, art.getId());
                        }
                });

            jtm.update(BORRAR_ARTICULO, periodico.getId());

            jtm.update(BORRAR_SUSCRIPCION, periodico.getId());

           resultSetInt = jtm.update(BORRAR_PERIODICO, periodico.getId());

            transactionManager.commit(txStatus);
        } catch (Exception e) {
            log.error(e);
        }
        if (resultSetInt == 1) {
            resultado.set(Either.right(true));
        } else {
            resultado.set(Either.left("Error al borrar"));
        }
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> addPeriodico(Periodico p) {

        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(INSERTAR_PERIODICO,
                            new Object[]{p.getId(), p.getNombre(), p.getPrecio_diario(), p.getDirector(), p.getId_administrador()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });

        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }


    @Override
    public Either<String, List<Periodico>> getTodosPeriodicosUsuario(Usuario u) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Periodico>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(SELECT_TODOS_PERIODICOS_USUARIO,
                            BeanPropertyRowMapper.newInstance(Periodico.class),
                            new Object[]{u.getId()}))
                            .onSuccess(periodicos ->
                                    resultado.set(Either.right(periodicos)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, List<Periodico>> getTodosPeriodicos() {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Periodico>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(SELECT_TODOS_PERIODICOS,
                            BeanPropertyRowMapper.newInstance(Periodico.class)))
                            .onSuccess(periodicos ->
                                    resultado.set(Either.right(periodicos)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> comprobarExistenciaporId(int id) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicReference p = new AtomicReference();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(PERIODICO_POR_ID,
                            BeanPropertyRowMapper.newInstance(Periodico.class),
                            new Object[]{id}))
                            .onSuccess(periodico ->
                                    p.set(periodico))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        if (p.get() != null) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }

    @Override
    public Either<String, Boolean> actualizarPeriodico(Periodico p) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(ACTUALIZAR_PERIODICO,
                            new Object[]{p.getNombre(), p.getPrecio_diario(), p.getDirector(), p.getId()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }

    @Override
    public Either<String, Periodico> devolverPeriodico(int id) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Periodico>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(PERIODICO_POR_ID,
                            BeanPropertyRowMapper.newInstance(Periodico.class),
                            new Object[]{id}))
                            .onSuccess(periodico ->
                                    resultado.set(Either.right(periodico)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
}
