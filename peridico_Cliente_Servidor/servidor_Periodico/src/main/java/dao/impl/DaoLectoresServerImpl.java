package dao.impl;

import dao.DBConnectionPool;
import dao.DaoLectoresServer;
import dao.DaoUsuariosServer;
import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoLectoresServerImpl implements DaoLectoresServer {
    private static String QUERY_INSERTAR_LECTOR =
            "insert into lectores  (id_lector,nombre, fechaNacimiento) " +
                    "values(?,?,?) ";
    private static String SELECT_UN_LECTOR =
            "select * from lectores l " +
                    "where l.id_lector = ? ";


    @Override
    public Either<String, Boolean> addLector(Lector l, Usuario u) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        DaoUsuariosServer daoUsuariosServer = new DaoUsuariosServerImpl();
        Either<String, Boolean> resultadoAddUusuario = daoUsuariosServer.addUsuario(u);
        Either<String, Usuario> resultadoUsuario = daoUsuariosServer.getUsuario(u.getUser(), u.getPassword());
        resultadoUsuario.peek(usuario -> {
            l.setId_lector(usuario.getId());
            resultadoAddUusuario.peek(opcion -> {
                if (opcion) {
                    Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                            .onSuccess(jtm -> {
                                Try.of(() -> jtm.update(QUERY_INSERTAR_LECTOR,
                                        new Object[]{l.getId_lector(), l.getNombre(), l.getFechaNacimiento()}))
                                        .onSuccess(i ->
                                                result.set(i))
                                        .onFailure(throwable -> {
                                            if (throwable instanceof DataAccessException) {
                                                resultado.set(Either.left("Datos erroneos"));
                                            } else {
                                                resultado.set(Either.left(throwable.getMessage()));
                                                log.error(throwable.getMessage(), throwable);
                                            }
                                        });
                            }).onFailure(throwable -> {
                        resultado.set(Either.left(throwable.getMessage()));
                        log.error(throwable.getMessage(), throwable);

                    });
                }
            });
        });
        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }

    @Override
    public Either<String, Lector> devolverLector(long id) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Lector>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(SELECT_UN_LECTOR,
                            BeanPropertyRowMapper.newInstance(Lector.class),
                            new Object[]{id}))
                            .onSuccess(lector ->
                                    resultado.set(Either.right(lector)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();
    }
}