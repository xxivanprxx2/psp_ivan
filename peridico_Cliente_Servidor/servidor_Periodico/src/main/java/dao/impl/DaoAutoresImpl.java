package dao.impl;

import dao.DBConnectionPool;
import dao.DaoAutores;
import dao.modelo.Autor;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoAutoresImpl implements DaoAutores {

    private static String INSERTAR_AUTOR =
            "insert into autores  (nombre,apellidos) " +
                    "values(?,?) ";

    private static String COMPROBAR_EXISTENCIA =
            "select * from autores a where a.nombre = ? and a.apellidos = ?";

    @Override
    public Either<String, Autor> comprobarExistencia(String nombre, String apellido) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Autor>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.queryForObject(COMPROBAR_EXISTENCIA,
                            BeanPropertyRowMapper.newInstance(Autor.class),
                            new Object[]{nombre, apellido}))
                            .onSuccess(autor ->
                                    resultado.set(Either.right(autor)))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);

                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable ->{
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });

        return resultado.get();    }


    @Override
    public Either<String, Boolean> addUsuario(Autor autor) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(INSERTAR_AUTOR,
                            new Object[]{autor.getNombre(), autor.getApellidos()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                    log.error(throwable.getMessage(), throwable);
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }
}
