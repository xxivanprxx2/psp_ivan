/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;


import com.zaxxer.hikari.HikariDataSource;
import config.ConfigurationServer;
import dao.DBConnectionPool;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author oscar
 */
public class DBConnectionPoolImpl implements DBConnectionPool {

    public static DBConnectionPoolImpl dbconection = null;

    private HikariDataSource hirakiDatasource = null;

    public DBConnectionPoolImpl() {
        hirakiDatasource = getDataSourceHikari();
        }

    @Override
    public Connection getConnection() throws Exception {
        Class.forName(ConfigurationServer.getInstance().getDriverDB());
        Connection connection;

        connection = hirakiDatasource.getConnection();

        return connection;
    }

    @Override
    public DataSource getDataSourceFromServer() throws NamingException {
        Context ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("jdbc/db4free");
        return ds;

    }

    @Override
    public DataSource getDataSource() {
        return hirakiDatasource;
    }

    @Override
    public void cerrarConexion(Connection connection) {
        try {
            if (connection != null) {
                connection.setAutoCommit(true);
                connection.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnectionPoolImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void cerrarPool() {
       hirakiDatasource.close();
    }
}
