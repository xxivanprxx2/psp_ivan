package dao.impl;

import dao.DBConnection;
import dao.DBConnectionPool;
import dao.DaoPeriodicos;
import dao.DaoSuscripciones;
import dao.modelo.*;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
public class DaoSuscripcionesImpl implements DaoSuscripciones {
    private static String SELECT_SUSCRIPCIONES_USUARIO =
            "select * from suscripciones s " +
                    "where s.id_lector = ?";
    private static String SELECT_SUSCRIPCIONES_PERIODICO =
            "select * from suscripciones s " +
                    "inner join lectores l on s.id_lector = l.id " +
                    "inner join periodicos p on s.id_periodico = p.id " +
                    "where p.id = ?";
    private static String ADD_SUSCRIPCION =
            "insert into suscripciones (id_lector,id_periodico,fecha_inicio,fecha_baja)" +
                    "values(?,?,?,?)";
    private static String DEVOLVER_SUSCRIPCION_ID =
            "select * from suscripciones s " +
                    "inner join lectores l on s.id_lector = l.id " +
                    "inner join periodicos p on s.id_periodico = p.id " +
                    "where s.id = ?";
    private static String BORRAR_SUSCRIPCION =
            "delete from suscripciones s where s.id_lector = ? and s.id_periodico = ? ";

    @Override
    public Either<String, Boolean> addSuscripcion(Suscripcion s) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        AtomicInteger result = new AtomicInteger();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.update(ADD_SUSCRIPCION,
                            new Object[]{s.getId_lector(),s.getId_periodico(),s.getFecha_inicio(),s.getFecha_baja()}))
                            .onSuccess(i ->
                                    result.set(i))
                            .onFailure(throwable -> {
                                if (throwable instanceof DataAccessException) {
                                    resultado.set(Either.left("Datos erroneos"));
                                } else {
                                    resultado.set(Either.left(throwable.getMessage()));
                                    log.error(throwable.getMessage(), throwable);
                                }
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });

        if (result.get() == 1) {
            resultado.set(Either.right(true));
        }
        return resultado.get();
    }


    @Override
    public Either<String, List<Periodico>> devolverPeridicosSuscripcionesUsuario(Lector l) {
        DaoPeriodicos daoPeriodicos = new DaoPeriodicosImpl();
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Periodico>>> resultado = new AtomicReference<>();
        List<Suscripcion> suscripciones = new ArrayList<>();
        List<Periodico> periodicosSuscripciones = new ArrayList<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(SELECT_SUSCRIPCIONES_USUARIO,
                            BeanPropertyRowMapper.newInstance(Suscripcion.class),
                            new Object[]{l.getId_lector()}))
                            .onSuccess(s ->
                                    suscripciones.addAll(s))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        for(Suscripcion s : suscripciones) {
            periodicosSuscripciones.add(daoPeriodicos.devolverPeriodico(s.getId_periodico()).get());
        }
        if(periodicosSuscripciones != null) {
            resultado.set(Either.right(periodicosSuscripciones));
        }
        return resultado.get();
    }


    @Override
    public Either<String, List<Suscripcion>> devolverTodasSuscripcionesUsuario(Lector l) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Suscripcion>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(SELECT_SUSCRIPCIONES_USUARIO,
                            BeanPropertyRowMapper.newInstance(Suscripcion.class),
                            new Object[]{l.getId_lector()}))
                            .onSuccess(suscripciones ->
                                    resultado.set(Either.right(suscripciones)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();

    }
    @Override
    public Either<String, List<Suscripcion>> devolverTodasSuscripciones(Periodico p) {
        DBConnectionPool dbConnectionPool = DBConnectionPool.getInstance();
        AtomicReference<Either<String, List<Suscripcion>>> resultado = new AtomicReference<>();
        Try.of(() -> new JdbcTemplate(dbConnectionPool.getDataSourceHikari()))
                .onSuccess(jtm -> {
                    Try.of(() -> jtm.query(SELECT_SUSCRIPCIONES_PERIODICO,
                            BeanPropertyRowMapper.newInstance(Suscripcion.class),
                            new Object[]{p.getId()}))
                            .onSuccess(suscripciones ->
                                    resultado.set(Either.right(suscripciones)))
                            .onFailure(throwable -> {
                                resultado.set(Either.left(throwable.getMessage()));
                                log.error(throwable.getMessage(), throwable);
                            });
                }).onFailure(throwable -> {
            resultado.set(Either.left(throwable.getMessage()));
            log.error(throwable.getMessage(), throwable);
        });
        return resultado.get();

    }

    @Override
    public Either<String, Boolean> borrarSuscricion(Usuario usuario, Periodico periodico) {
        DBConnection db = new DBConnectionImpl();
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();
        JdbcTemplate jtm = new JdbcTemplate(
                db.getDataSource());
        int resultSetInt = 0;
        try {

            resultSetInt = jtm.update(BORRAR_SUSCRIPCION,
                    usuario.getId(),
                    periodico.getId());
        } catch (Exception e) {
            log.error(e);
        }
        if (resultSetInt == 1) {
            resultado.set(Either.right(true));
        } else {
            resultado.set(Either.left("Error al borrar"));
        }
        return resultado.get();
    }
}
