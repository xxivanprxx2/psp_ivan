package dao;

import dao.modelo.Usuario;
import io.vavr.control.Either;
import lombok.SneakyThrows;

import java.util.List;

public interface DaoUsuariosServer {
    Either<String, Usuario> getUsuario(String user, String hasPassword);

    Either<String, Boolean> actualizarUsuario(Usuario usuario);

    Either<String, Boolean> addUsuario(Usuario usuario);

    Either<String, Usuario> getUsuarioUser(String mail);

    Either<String, List<Usuario>> devolverTodosLectores();

    Either<String, Boolean> borrarLector(Usuario usuario);

}
