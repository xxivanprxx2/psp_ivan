package dao;

import dao.modelo.*;
import io.vavr.control.Either;

import java.time.LocalDate;
import java.util.List;

public interface DaoInformes {
    Either<String, InformeTipo1> ratingMedioPeriodicos(int id);

    Either<String, InformeTipo1> ratingMedioAutor(int id);

    Either<String, List<InformeTipo1>> listadoAutores();

    Either<String, List<Autor>> listadoAutoresValor(double valor);

    Either<String, InformeTipo1> autoresMasRting();

    Either<String, List<InformeTipo1>> tipoArticulosOrdenados();

    Either<String, InformesTipo2> periodicoMasSuscripcionesDia(LocalDate date);

    Either<String, InformesTipo3> periodicoMasGanames();
}
