package servicios;

import dao.modelo.Autor;
import io.vavr.control.Either;

public interface ServiciosAutoresServer {
    Either<String, Autor> comprobarExistencia(String nombre, String apellido);

    Either<String, Boolean> addAutor(Autor a);
}
