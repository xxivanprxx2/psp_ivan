package servicios.impl;

import dao.DaoArticulos;
import dao.impl.DaoArticulosImpl;
import dao.modelo.Articulo;
import dao.modelo.Tipo_articulo;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosArticulosServer;

import javax.inject.Inject;
import java.util.List;

public class ServiciosArticulosServerImpl implements ServiciosArticulosServer {


    @Override
    public Either<String, List<Tipo_articulo>> devolerTodosTiposArticulos(){
        DaoArticulos daoArticulos = new DaoArticulosImpl();
        return daoArticulos.devolverTodosTiposArticulos();
    }


    @Override
    public Either<String, Boolean> addArticulo(Articulo articulo){
        DaoArticulos daoArticulos = new DaoArticulosImpl();
        return daoArticulos.addArticulo(articulo);
    }


    @Override
    public Either<String, List<Articulo>> devolverArticulosPeriodico(int id){
        DaoArticulos daoArticulos = new DaoArticulosImpl();
        return daoArticulos.devolverTodosArticulosPeriodico(id);
    }
}
