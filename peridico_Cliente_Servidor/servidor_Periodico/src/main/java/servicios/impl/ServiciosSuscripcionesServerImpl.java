package servicios.impl;

import dao.DaoSuscripciones;
import dao.impl.DaoSuscripcionesImpl;
import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosSuscripcionesServer;

import java.util.List;

public class ServiciosSuscripcionesServerImpl implements ServiciosSuscripcionesServer {

    @Override
    public Either<String, Boolean> addSuscripcion(Suscripcion suscripcion){
        DaoSuscripciones daoSuscripciones = new DaoSuscripcionesImpl();
        return daoSuscripciones.addSuscripcion(suscripcion);
    }


    @Override
    public Either<String, List<Periodico>> devolverPeriodicosSuscripcionesLector(Lector lector){
        DaoSuscripciones daoSuscripciones = new DaoSuscripcionesImpl();
        return daoSuscripciones.devolverPeridicosSuscripcionesUsuario(lector);
    }
    @Override
    public Either<String, List<Suscripcion>> devolverSuscripcionesLector(Lector lector){
        DaoSuscripciones daoSuscripciones = new DaoSuscripcionesImpl();
        return daoSuscripciones.devolverTodasSuscripcionesUsuario(lector);
    }
    @Override
    public Either<String, List<Suscripcion>> devolverSuscripcionesPeriodico(Periodico periodico){
        DaoSuscripciones daoSuscripciones = new DaoSuscripcionesImpl();
        return daoSuscripciones.devolverTodasSuscripciones(periodico);
    }
    @Override
    public Either<String, Boolean> borrarSuscripcion(Usuario usuario, Periodico periodico){
        DaoSuscripciones daoSuscripciones = new DaoSuscripcionesImpl();
        return daoSuscripciones.borrarSuscricion(usuario, periodico);
    }
}
