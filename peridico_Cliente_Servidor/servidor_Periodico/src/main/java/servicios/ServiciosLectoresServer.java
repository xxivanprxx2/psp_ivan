package servicios;

import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;

public interface ServiciosLectoresServer {
    Either<String, Boolean> addLector(Lector lector, Usuario usuario);

    Either<String, Lector> devolverUnLector(long id);
}
