package servicios;

import dao.modelo.Usuario;
import io.vavr.control.Either;
import lombok.SneakyThrows;

import java.util.List;

public interface ServiciosUsuarioServer {
    @SneakyThrows
    Either<String, Usuario> comprobarUsuarioServer(String usuario, String pass);

    Either<String, Boolean> actualizarUsuario(Usuario usuario);

    @SneakyThrows
    Either<String, Usuario> comprobarUsuarioMail(String mail);
    @SneakyThrows
    Either<String, List<Usuario>> devolverTodosLectores();

    Either<String, Boolean> borrarLector(Usuario usuario);

    Either<String, Boolean> addUsuario(Usuario usuario);
}
