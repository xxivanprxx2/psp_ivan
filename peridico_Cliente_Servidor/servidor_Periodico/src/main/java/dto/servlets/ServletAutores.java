package dto.servlets;

import dao.modelo.Autor;
import dao.modelo.Periodico;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosAutoresServer;
import servicios.ServiciosPeriodicosServer;
import servicios.impl.ServiciosAutoresServerImpl;
import servicios.impl.ServiciosPeriodicosServerImpl;
import utils.Constantes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServletAutores", urlPatterns = {"/Autor"})
public class ServletAutores extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ADD_AUTOR:
                addAutor(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_COMPROBAR_EXIXTENCIA_AUTOR:
                devolverPeriodicosUser(request, response);
                break;
        }
    }

    private void devolverPeriodicosUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosAutoresServer serviciosAutoresServer = new ServiciosAutoresServerImpl();
        Autor autor = (Autor) request.getAttribute("Autor");
        if (autor != null) {
            Either<String, Autor> respuesta = serviciosAutoresServer.comprobarExistencia(autor.getNombre(),autor.getApellidos());
            respuesta.peek(autor1 -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, autor1);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }

    private void addAutor(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosAutoresServer serviciosAutoresServer = new ServiciosAutoresServerImpl();
        Autor autor = (Autor) request.getAttribute("Autor");
        if (autor != null) {
            Either<String, Boolean> respuesta = serviciosAutoresServer.addAutor(autor);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, opcion);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }
}
