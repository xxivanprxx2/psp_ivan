package dto.servlets;

import dao.modelo.Articulo;
import dao.modelo.ArticuloLeido;
import dao.modelo.Periodico;
import io.vavr.control.Either;
import servicios.ServiciosArticulosLeidos;
import servicios.ServiciosArticulosServer;
import servicios.ServiciosPeriodicosServer;
import servicios.impl.ServiciosArticulosLeidosIImpl;
import servicios.impl.ServiciosArticulosServerImpl;
import servicios.impl.ServiciosPeriodicosServerImpl;
import utils.Constantes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletArticulos_leidos", urlPatterns = {"/ArticuloLeido"})
public class ServletArticulos_leidos extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ADD_ARTICULO_LEIDO:
                addArticuloLeido(request, response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_DEVOLVER_ARTICULO_LEIDO:
                devolverArticuloLeido(request, response);
                break;
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ACTUALIZAR_ARTICULO_LEIDO:
                actualizarArticuloLeido(req, resp);
                break;
        }
    }

    private void devolverArticuloLeido(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosArticulosLeidos serviciosArticulosLeidos = new ServiciosArticulosLeidosIImpl();
        ArticuloLeido articuloLeido = (ArticuloLeido) request.getAttribute("ArticuloLeido");
        if (articuloLeido != null) {
            Either<String, ArticuloLeido> respuesta = serviciosArticulosLeidos.devolverArticuloLeido(articuloLeido.getId_articulo(),articuloLeido.getId_lector());
            respuesta.peek(a -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, a);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        }else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }
    private void addArticuloLeido(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosArticulosLeidos serviciosArticulosLeidos = new ServiciosArticulosLeidosIImpl();
        ArticuloLeido articuloLeido = (ArticuloLeido) request.getAttribute("ArticuloLeido");
        if (articuloLeido != null) {
            Either<String, Boolean> respuesta = serviciosArticulosLeidos.addArticulo(articuloLeido);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, opcion);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }

    private void actualizarArticuloLeido(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosArticulosLeidos serviciosArticulosLeidos = new ServiciosArticulosLeidosIImpl();
        ArticuloLeido articuloLeido = (ArticuloLeido) request.getAttribute("ArticuloLeido");
        if (articuloLeido != null) {
            Either<String, Boolean> respuesta = serviciosArticulosLeidos.actualizarRating(articuloLeido.getRating(),articuloLeido.getId());
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, true);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Articulo no valido");
        }
    }
}
