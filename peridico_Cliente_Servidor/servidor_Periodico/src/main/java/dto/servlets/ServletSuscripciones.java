package dto.servlets;

import dao.modelo.Lector;
import dao.modelo.Periodico;
import dao.modelo.Suscripcion;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosSuscripcionesServer;
import servicios.ServiciosUsuarioServer;
import servicios.impl.ServiciosSuscripcionesServerImpl;
import servicios.impl.ServiciosUsuarioServerImpl;
import utils.Constantes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServletSuscripciones", urlPatterns = {"/Suscripcion"})
public class ServletSuscripciones extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ADD_SUSCRIPCION:
                addSuscripcion(request, response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_DEVOLVER_PERIODICOS_SUSCRIPCIONES_USUARIO:
                devolverPeriodicosSuscripcionesUser(request, response);
                break;
            case Constantes.URL_DEVOLVER_SUSCRIPCIONES_USUARIO:
                devolverSuscripcionesUser(request, response);
                break;
            case Constantes.URL_DEVOLVER_TODAS_SUSCRIPCIONES:
                devolverSuscripciones(request, response);
                break;
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_BORRAR_SUSCRIPCION:
                borrarSuscripcion(req, resp);
                break;
        }    }

    private void borrarSuscripcion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosSuscripcionesServer serviciosSuscripcionesServer = new ServiciosSuscripcionesServerImpl();
        Usuario user = (Usuario) request.getAttribute("Usuario");
        Periodico periodico = (Periodico)request.getAttribute("Periodico");
        if (user != null) {
            if(periodico != null) {
                Either<String, Boolean> respuesta = serviciosSuscripcionesServer.borrarSuscripcion(user, periodico);
                respuesta.peek(opcion -> {
                    request.setAttribute(Constantes.PARAM_RESPUESTA, true);
                }).peekLeft(s -> {
                    request.setAttribute(Constantes.PARAM_ERROR, s);
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                });
            }else{
                request.setAttribute(Constantes.PARAM_ERROR, "Periodico no valido");
            }
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Usuario no valido");
        }
    }
    private void devolverPeriodicosSuscripcionesUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosSuscripcionesServer serviciosSuscripcionesServer = new ServiciosSuscripcionesServerImpl();
        Lector lector = (Lector) request.getAttribute("lector");

        if (lector != null) {
            Either<String, List<Periodico>> respuesta = serviciosSuscripcionesServer.devolverPeriodicosSuscripcionesLector(lector);
            respuesta.peek(periodicos -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, periodicos);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }
    private void devolverSuscripcionesUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosSuscripcionesServer serviciosSuscripcionesServer = new ServiciosSuscripcionesServerImpl();
        Lector lector = (Lector) request.getAttribute("lector");

        if (lector != null) {
            Either<String, List<Suscripcion>> respuesta = serviciosSuscripcionesServer.devolverSuscripcionesLector(lector);
            respuesta.peek(periodicos -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, periodicos);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }
    private void devolverSuscripciones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosSuscripcionesServer serviciosSuscripcionesServer = new ServiciosSuscripcionesServerImpl();
        Periodico periodico = (Periodico) request.getAttribute("Periodico");

        if (periodico != null) {
            Either<String, List<Suscripcion>> respuesta = serviciosSuscripcionesServer.devolverSuscripcionesPeriodico(periodico);
            respuesta.peek(suscripcions -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, suscripcions);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }

    private void addSuscripcion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosSuscripcionesServer serviciosSuscripcionesServer = new ServiciosSuscripcionesServerImpl();
        Suscripcion suscripcion = (Suscripcion) request.getAttribute("Suscripcion");
        if (suscripcion != null) {
            Either<String, Boolean> respuesta = serviciosSuscripcionesServer.addSuscripcion(suscripcion);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, opcion);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Erroneos");
        }
    }
}
