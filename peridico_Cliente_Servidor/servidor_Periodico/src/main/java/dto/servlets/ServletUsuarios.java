package dto.servlets;

import config.ListenerConfig;
import dao.modelo.Lector;
import dao.modelo.Usuario;
import io.vavr.control.Either;
import servicios.ServiciosLectoresServer;
import servicios.ServiciosUsuarioServer;
import servicios.impl.ServiciosLectoresServerImpl;
import servicios.impl.ServiciosUsuarioServerImpl;
import utils.Constantes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServletUsuarios", urlPatterns = {"/Usuario"})
public class ServletUsuarios extends HttpServlet {

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ACTUALIZAR_USUARIO:
                actualizarUsuario(req, resp);
                break;
            case Constantes.URL_BORRAR_LECTOR:
                borrarLector(req, resp);
                break;
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_ADD_LECTOR:
                addLector(request, response);
                break;
            case Constantes.URL_ADD_USUARIO:
                addUsuario(request, response);
                break;
            case Constantes.URL_LOGOUT:
                logout(request, response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch (request.getParameter(Constantes.PARAM_ACCION)) {
            case Constantes.URL_LOGGING:
                hacerLogging(request, response);
                break;
            case Constantes.URL_COMPROBAR_MAIL:
                comprobarMail(request, response);
                break;
            case Constantes.URL_DEVOLVER_LECTORES:
                devolverLectores(request, response);
                break;
            case Constantes.URL_DEVOLVER_LECTOR:
                devolverlector(request, response);
                break;
        }
    }
    private void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("usuarioSesion",null);
        request.getSession().invalidate();
    }
    private void hacerLogging(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosUsuarioServer serviciosUsuariosServer = new ServiciosUsuarioServerImpl();
        Usuario user = (Usuario) request.getAttribute("Usuario");

        if (user != null) {
            String nombre = user.getUser();
            String password = user.getPassword();
            Either<String, Usuario> respuesta = serviciosUsuariosServer.comprobarUsuarioServer(nombre, password);
            respuesta.peek(usuario -> {
                request.getSession().setAttribute("usuarioSesion", usuario);
                request.setAttribute(Constantes.PARAM_RESPUESTA, usuario);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }


    private void comprobarMail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosUsuarioServer serviciosUsuariosServer = new ServiciosUsuarioServerImpl();
        Usuario user = (Usuario) request.getAttribute("Usuario");
        if (user != null) {
            String mail = user.getMail();
            Either<String, Usuario> respuesta = serviciosUsuariosServer.comprobarUsuarioMail(mail);
            respuesta.peek(usuario -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, usuario);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }

    private void devolverLectores(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosUsuarioServer serviciosUsuariosServer = new ServiciosUsuarioServerImpl();
        Either<String, List<Usuario>> respuesta = serviciosUsuariosServer.devolverTodosLectores();
        respuesta.peek(lectores -> {
            request.setAttribute(Constantes.PARAM_RESPUESTA, lectores);
        }).peekLeft(s -> {
            request.setAttribute(Constantes.PARAM_ERROR, s);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        });
    }

    private void actualizarUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosUsuarioServer serviciosUsuariosServer = new ServiciosUsuarioServerImpl();
        Usuario user = (Usuario) request.getAttribute("Usuario");
        if (user != null) {
            Either<String, Boolean> respuesta = serviciosUsuariosServer.actualizarUsuario(user);
            respuesta.peek(opcion -> {
                request.getSession().setAttribute("usuarioSesion", user);
                request.setAttribute(Constantes.PARAM_RESPUESTA, true);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Usuario no valido");
        }
    }

    private void borrarLector(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosUsuarioServer serviciosUsuariosServer = new ServiciosUsuarioServerImpl();
        Usuario user = (Usuario) request.getAttribute("Usuario");
        if (user != null) {
            Either<String, Boolean> respuesta = serviciosUsuariosServer.borrarLector(user);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, true);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Usuario no valido");
        }
    }

    private void addUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosUsuarioServer serviciosUsuarioServer = new ServiciosUsuarioServerImpl();
        Usuario usuario = (Usuario) request.getAttribute("Usuario");
        if (usuario != null) {
            Either<String, Boolean> respuesta = serviciosUsuarioServer.addUsuario(usuario);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, true);
            }).peekLeft(s ->
            {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        }
    }

    private void addLector(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosLectoresServer serviciosLectoresServer = new ServiciosLectoresServerImpl();
        Lector lector = (Lector) request.getAttribute("lector");
        if (lector != null) {
            Usuario user = new Usuario(lector.getUser()
                    , lector.getPassword()
                    , lector.getPrimera_vez()
                    , lector.getMail()
                    , lector.getId_tipo_usuario());

            Either<String, Boolean> respuesta = serviciosLectoresServer.addLector(lector, user);
            respuesta.peek(opcion -> {
                request.setAttribute(Constantes.PARAM_RESPUESTA, true);
            }).peekLeft(s ->
            {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        }
    }
    private void devolverlector(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServiciosLectoresServer serviciosLectoresServer = new ServiciosLectoresServerImpl();
        Lector lector = (Lector) request.getAttribute("lector");
        if (lector != null) {
            Either<String, Lector> respuesta = serviciosLectoresServer.devolverUnLector(lector.getId_lector());
            respuesta.peek(usuario -> {
                request.getSession().setAttribute("usuarioSesion", usuario);
                request.setAttribute(Constantes.PARAM_RESPUESTA, usuario);
            }).peekLeft(s -> {
                request.setAttribute(Constantes.PARAM_ERROR, s);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            });
        } else {
            request.setAttribute(Constantes.PARAM_ERROR, "Campos Vacios");
        }
    }
}
