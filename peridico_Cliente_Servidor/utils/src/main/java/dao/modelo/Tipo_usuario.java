package dao.modelo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class Tipo_usuario {
    private int id_tipo_usuario;
    private  String tipo;
    private  String descripcion;

    public Tipo_usuario(int id_tipo_usuario, String tipo, String descripcion) {
        this.id_tipo_usuario = id_tipo_usuario;
        this.tipo = tipo;
        this.descripcion = descripcion;
    }
}
