package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Usuario {
    private long id;
    private  String user;
    private  String password;
    private  int primera_vez;
    private  String mail;
    private  int id_tipo_usuario;

    public Usuario(String user, String password, int primera_vez, String mail, int id_tipo_usuario) {
        this.user = user;
        this.password = password;
        this.primera_vez = primera_vez;
        this.mail = mail;
        this.id_tipo_usuario = id_tipo_usuario;
    }


    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", primera_vez=" + primera_vez +
                ", mail='" + mail + '\'' +
                ", tipo_usuario=" + id_tipo_usuario +
                '}';
    }
}
