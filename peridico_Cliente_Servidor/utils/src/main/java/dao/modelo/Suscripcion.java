package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Suscripcion {
    private int id;
    private int id_lector;
    private int id_periodico;
    private Date fecha_inicio;
    private Date fecha_baja;

    public Suscripcion(int id_lector, int id_periodico, Date fecha_inicio, Date fecha_baja) {
        this.id_lector = id_lector;
        this.id_periodico = id_periodico;
        this.fecha_inicio = fecha_inicio;
        this.fecha_baja = fecha_baja;
    }

    @Override
    public String toString() {
        return "Suscripcion{" +
                "id=" + id +
                ", id_lector=" + id_lector +
                ", id_periodico=" + id_periodico +
                ", fecha_inicio=" + fecha_inicio +
                ", fecha_baja=" + fecha_baja +
                '}';
    }
}
