package dao.modelo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class Periodico {
    private int id;
    private String nombre;
    private double precio_diario;
    private String director;
    private long id_administrador;

    public Periodico(int id,String nombre, double precio_diario, String director, long id_administrador) {
        this.id = id;
        this.nombre = nombre;
        this.precio_diario = precio_diario;
        this.director = director;
        this.id_administrador = id_administrador;
    }


    @Override
    public String toString() {
        return "Periodico{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", precio_diario=" + precio_diario +
                ", director='" + director + '\'' +
                ", id_administrador=" + id_administrador +
                '}';
    }
}
