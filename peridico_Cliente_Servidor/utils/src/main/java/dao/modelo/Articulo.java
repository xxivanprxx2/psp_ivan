package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Articulo {
    private int id;
    private String titular;
    private String descripcion;
    private int id_periodico;
    private int id_tipo;
    private int id_autor;

    public Articulo(String titular, String descripcion, int id_periodico, int id_tipo, int id_autor) {
        this.titular = titular;
        this.descripcion = descripcion;
        this.id_periodico = id_periodico;
        this.id_tipo = id_tipo;
        this.id_autor = id_autor;
    }

    @Override
    public String toString() {
        return "Articulo{" +
                "titular='" + titular + '\'' +
                '}';
    }
}
