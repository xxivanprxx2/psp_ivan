package dao.modelo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Lector extends Usuario{
    private long id_lector;
    private String nombre;
    private LocalDate fechaNacimiento;

    public Lector(String user, String password, int primera_vez, String mail, int id_tipo_usuario, String nombre, LocalDate fechaNacimiento) {
        super(user, password, primera_vez, mail, id_tipo_usuario);
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "Lector{" +
                "id_lector=" + id_lector +
                ", nombre='" + nombre + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                '}';
    }
}
