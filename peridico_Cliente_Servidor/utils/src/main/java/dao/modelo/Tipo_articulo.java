package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Tipo_articulo {
    private long id;
    private String tipo;

    @Override
    public String toString() {
        return "Tipo_articulo{" +
                "tipo='" + tipo + '\'' +
                '}';
    }
}
