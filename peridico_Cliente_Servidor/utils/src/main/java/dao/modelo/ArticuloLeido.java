package dao.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ArticuloLeido {
    private int id;
    private int id_lector;
    private int id_articulo;
    private int rating;

    public ArticuloLeido(int id_lector, int id_articulo, int rating) {
        this.id_lector = id_lector;
        this.id_articulo = id_articulo;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "ArticuloLeido{" +
                "id=" + id +
                ", id_lector=" + id_lector +
                ", id_articulo=" + id_articulo +
                ", rating=" + rating +
                '}';
    }
}
