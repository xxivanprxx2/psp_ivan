package gui.controllers;

import config.Configuration;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import okhttp3.*;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class PantallaLoggingController implements Initializable {
    @FXML
    private TextField textUsuario;
    @FXML
    private TextField textPassword;

    private Alert alert;
    private PantallaPrincipalController borderPane;

    public void setBorderPane(PantallaPrincipalController borderPane) {
        this.borderPane = borderPane;
    }

    public void botonAceptar(ActionEvent actionEvent) {
        CookieManager cookieManager = new CookieManager();
        OkHttpClient clientOk = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .build();
        borderPane.setOkHttpClient(clientOk);
        String url = Configuration.getInstance().getUrl() + "logging";
        RequestBody formBody = new FormBody.Builder()
                .add("usuario", textUsuario.getText())
                .add("password", textPassword.getText())
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        try {
            Response response = clientOk
                    .newCall(request)
                    .execute();
            if (response.isSuccessful()) {
                String respuesta = response.body().string();
                if (respuesta.equals("No valido")) {
                    alert.setContentText("Error: Campos no validos");
                    alert.show();
                }else{
                    String[] strings = respuesta.split(",");
                    borderPane.getProductos().addAll(Arrays.asList(strings));
                    borderPane.cargarProductos();
                }
            }
        } catch (Exception e) {
            alert.setContentText("Error al conectar");
            alert.show();
        }
        clientOk.connectionPool().evictAll();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alert = new Alert(Alert.AlertType.ERROR);
    }
}
