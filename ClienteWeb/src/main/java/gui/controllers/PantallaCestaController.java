package gui.controllers;

import config.Configuration;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import okhttp3.*;


import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class PantallaCestaController implements Initializable {
    @FXML
    private ListView<String> listCesta;

    private PantallaPrincipalController borderPane;

    public ListView<String> getListCesta() {
        return listCesta;
    }
    private Alert alert;
    public void setBorderPane(PantallaPrincipalController borderPane) {
        this.borderPane = borderPane;
    }

    private OkHttpClient clientOk;

    public void setClientOk(OkHttpClient clientOk) {
        this.clientOk = clientOk;
    }

    public void botonComprar(ActionEvent actionEvent) {
        clientOk = borderPane.getOkHttpClient();
        String url = Configuration.getInstance().getUrl() + "cesta";
        RequestBody formBody = new FormBody.Builder()
                .add("op", "limpiar")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        try {
            Response response = clientOk
                    .newCall(request)
                    .execute();
            if (response.isSuccessful()) {
                String respuesta = response.body().string();
                borderPane.getCesta().clear();
                borderPane.getCesta().add(respuesta);
                listCesta.getItems().clear();
                listCesta.getItems().addAll(borderPane.getCesta());
            }
        } catch (Exception e) {
            alert.setContentText("Error al conectar");
            alert.show();
        }
        clientOk.connectionPool().evictAll();
    }

    public void bootnVolver(ActionEvent actionEvent) {
        clientOk = borderPane.getOkHttpClient();
        String url = Configuration.getInstance().getUrl() + "cesta";
        RequestBody formBody = new FormBody.Builder()
                .add("op", "volver")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        try {
            Response response = clientOk
                    .newCall(request)
                    .execute();
            if (response.isSuccessful()) {
                String respuesta = response.body().string();
                borderPane.getProductos().clear();
                String[] productos = respuesta.split(",");
                borderPane.getProductos().addAll(Arrays.asList(productos));
                borderPane.cargarProductos();
            }
        } catch (Exception e) {
            alert.setContentText("Error al conectar");
            alert.show();
        }
        clientOk.connectionPool().evictAll();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alert = new Alert(Alert.AlertType.ERROR);
    }
}
