package gui.controllers;

import config.Configuration;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import okhttp3.*;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class PantallaProductosController implements Initializable {
    @FXML
    private ListView<String> listProductos;

    private PantallaPrincipalController borderPane;

    public void setBorderPane(PantallaPrincipalController borderPane) {
        this.borderPane = borderPane;
    }

    public ListView<String> getProductos() {
        return listProductos;
    }
    private Alert alert;
    public void botonAdd(ActionEvent actionEvent) {
        OkHttpClient clientOk = borderPane.getOkHttpClient();
        String url = Configuration.getInstance().getUrl() + "producto";
        List<String> productos = listProductos.getSelectionModel().getSelectedItems();
        RequestBody formBody = new FormBody.Builder()
                .add("producto", String.valueOf(productos))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        try {
            Response response = clientOk
                    .newCall(request)
                    .execute();
            if (response.isSuccessful()) {
                String respuesta = response.body().string();
                String[] cesta = respuesta.split(",");
                borderPane.getCesta().clear();
                borderPane.getCesta().addAll(Arrays.asList(cesta));
                borderPane.cargarCesta();
            }
        } catch (Exception e) {
            alert.setContentText("Error al conectar");
            alert.show();
        }
        clientOk.connectionPool().evictAll();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
     listProductos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        alert = new Alert(Alert.AlertType.ERROR);
    }
}
