package gui.controllers;

import config.Configuration;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import lombok.SneakyThrows;
import okhttp3.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class PantallaPrincipalController implements Initializable {

    private PantallaLoggingController pantallaLoggingController;
    private AnchorPane pantallaLogging;

    private PantallaProductosController pantallaProductosController;
    private AnchorPane pantallaProductos;

    private PantallaCestaController pantallaCestaController;
    private AnchorPane pantallaCesta;

    @FXML
    private BorderPane borderPane;

    @FXML
    private MenuBar menuBar;

    OkHttpClient okHttpClient;

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public void setOkHttpClient(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    private List<String> productos = new ArrayList<>();

    private List<String> cesta =new ArrayList<>();

    public List<String> getCesta() {
        return cesta;
    }

    public List<String> getProductos() {
        return productos;
    }

    private Alert alert;
    @SneakyThrows
    public void cargarLogging() {
        if (pantallaLogging == null) {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource("/fxml/pantallaLogging.fxml"));
            pantallaLogging = loaderMenu.load();
            pantallaLoggingController = loaderMenu.getController();
            pantallaLoggingController.setBorderPane(this);
        }
        borderPane.setCenter(pantallaLogging);
    }


    @SneakyThrows
    public void cargarProductos() {
        menuBar.setVisible(true);
        if (pantallaProductos == null) {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource("/fxml/pantallaProductos.fxml"));
            pantallaProductos = loaderMenu.load();
            pantallaProductosController = loaderMenu.getController();
            pantallaProductosController.setBorderPane(this);
        }
        pantallaProductosController.getProductos().getItems().clear();
        pantallaProductosController.getProductos().getItems().addAll(productos);
        borderPane.setCenter(pantallaProductos);
    }

    public void menuBotonCesta(ActionEvent actionEvent) {
        cargarCesta();
    }
    @SneakyThrows
    public void cargarCesta() {
        if (pantallaCesta == null) {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource("/fxml/pantallaCesta.fxml"));
            pantallaCesta = loaderMenu.load();
            pantallaCestaController = loaderMenu.getController();
            pantallaCestaController.setBorderPane(this);
            pantallaCestaController.setClientOk(okHttpClient);
        }
        pantallaCestaController.getListCesta().getItems().clear();
        pantallaCestaController.getListCesta().getItems().addAll(cesta);
        borderPane.setCenter(pantallaCesta);
    }
    public void menuBotonLogout(ActionEvent actionEvent) {
        OkHttpClient clientOk = okHttpClient;
        String url = Configuration.getInstance().getUrl() + "logout";
        RequestBody formBody = new FormBody.Builder()
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        try {
            Response response = clientOk
                    .newCall(request)
                    .execute();
            if (response.isSuccessful()) {
                menuBar.setVisible(false);
                productos.clear();
                cesta.clear();
                cargarLogging();
            }
        } catch (Exception e) {
            alert.setContentText("Error al conectar");
            alert.show();
        }
        clientOk.connectionPool().evictAll();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        cargarLogging();
        menuBar.setVisible(false);
        alert = new Alert(Alert.AlertType.ERROR);

    }
}
